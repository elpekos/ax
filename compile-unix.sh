#!/bin/bash

# *************************************************************************************
#
# AX utility compilation script
#
# *************************************************************************************


command -v gcc >/dev/null 2>&1 || { echo "gcc not found on system" >&2; exit 1; }
echo Compiling AX utility
gcc ax.c -o ax
chmod +x ax
mv ./ax /Applications/Z88AsmWorkbench
echo Done

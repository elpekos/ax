**AX** is a utility for extracting API comments in assembly source files.
It is designed to work with the [OZ operating system project](https://bitbucket.org/cambridge/oz/src/develop/).
It generates a markdown file with a table of content.

[OZ operating system API](api.md)

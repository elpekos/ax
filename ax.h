// AX
// my Z80 assembler API extractor
//
// T.Peycru, 2023-25

// max constants
#define C_MAX  200      // buffer for fgets (one line from asm source file)
#define I_MAX 1000      // number of entries
#define R_MAX  200      // buffer for entry reference
#define A_MAX  200      // buffer for entry anchor
#define N_MAX  200      // buffer for entry name
#define T_MAX 9000      // buffer for entry text 

// finite state machine
#define FSM_SCAN 0      // parsing file until trigger found
#define FSM_LINE 1      // looking for first line, defines sorting reference
#define FSM_TEXT 2      // filling text of entry

// file extensions
#define LXEXT ".tex"
#define MDEXT ".md"
#define FN_MAX 200      // max size of generated output filename

// usage
const char *usage = "Usage: ax [-lmdh] input directory [output file]\n"
                    "Try -h for more\n";

// help
const char *help  = "\e[1mAPI Extractor - Thierry Peycru, 2023-25\e[m\n\n"
                    "Written for MPM Z80 asm sources, this utility builds a documentation from comments in source files.\n"
                    "Consecutive comments following the trigger sequence are extract and translated to Latex or Markdown.\n"
                    "The trigger is a sequence of LF+;@/  it is followed by a sorting key.\n"
                    "\nUsage:\n"
                    "ax [-lmdh] input directory [output file]\n"
                    "\nOptions:\n"
                    "-l : output file in Latex format (default)\n"
                    "-m : output file in Markdown format\n"
                    "-d : be verbose for debug\n"
                    "-h : this help\n";

// leading and tailing latex sequences
const char *lead =
                    "\\documentclass[oneside, a4paper, 10pt]{book}\n"
                    "\\usepackage{fancyhdr}\n"
                    "\\usepackage{listings}\n"
                    "\\usepackage[left=2cm, right=1cm, top=2cm, bottom=2cm]{geometry}\n"
                    "\\usepackage{color}\n"
                    "\\usepackage{inconsolata}\n"
                    "\\usepackage{sectsty}\n"
                    "\\usepackage{hyperref}\n"
                    
                    "\\allsectionsfont{\\ttfamily}\n"
                    "\\paragraphfont{\\ttfamily}\n"
                    "\\title{OZ Operating System API Reference}\n"
                    "\\author{Thierry Peycru}\n"
                    "\\date{\\today}\n"
                    "\\hypersetup{\n"
                    "    hyperindex=true,\n"
                    "    bookmarks=true,\n"
                    "    bookmarksopen=true,\n"
                    "    colorlinks=true,\n"
                    "    linkcolor=blue,\n"
                    "    filecolor=magenta,\n"
                    "    urlcolor=cyan,\n"
                    "    pdftitle={OZ Operating System API Reference},\n"
                    "    pdfauthor={Thierry Peycru}\n"
                    "}\n"
                    "\\lstdefinestyle{OZ}{\n"
                    "    belowcaptionskip=1\\baselineskip,\n"
                    "    breaklines=true,\n"
                    "    frame=none,\n"
                    "    numbers=none,\n"
                    "    extendedchars=true,\n"
                    "    keepspaces=true,\n"
                    "    xleftmargin=0pt,\n"
                    "    basicstyle=\\ttfamily\\small\n"
                    "}\n"
                    "\\lstset{style=OZ}\n"
                    "\\begin{document}\n"
                    //"\\texttt\n"
                    "\\maketitle\n"
                    "\\frontmatter\n"
                    "\\chapter*{Copyright Statement}\n"
                    "  Copyright {\\copyright} Thierry Peycru, \\the\\year.\n\n"
                    "  Permission is granted to copy, distribute and/or modify this document\n"
                    "  under the terms of the GNU Free Documentation License, Version 1.1\n"
                    "  or any later version published by the Free Software Foundation;\n"
                    "  with no Invariant Sections, with no Front-Cover Texts, and with no\n" 
                    "  Back-Cover Texts. A copy of the license is included in the section \n"
                    "  entitled ``GNU Free Documentation License''.\n"
                    "\\cleardoublepage\n"
                    "\\pdfbookmark[0]{\\contentsname}{Contents}\n"
                    "\\tableofcontents\n"
                    "\\mainmatter\n"
                    ;

const char *tail =
                    "\\end{document}\n"
                    ;

// entry counter
int i = 0;

// entry structure
struct entry
{
    char *ref;          // reference for sorting
    char *anch;         // anchor link
    char *name;         // name for title
    char *text;         // text of entry
};

// array of pointers to entries
struct entry *ndx[I_MAX];


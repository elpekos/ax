# OZ 5.0 System API References

[`DC`](#dc-calls-xx0c-director-and-cli)


[`DC_ALT`](#dc_alt-1a0c-pass-an-alternative-character) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_BYE`](#dc_bye-080c-exiting-current-application) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_ELF`](#dc_elf-260c-elf-low-level-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_ENV`](#dc_env-2a0c-system-variable-environment-api) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_GEN`](#dc_gen-200c-screen-driver-soh-call-toggle-redirection-to-printer) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_ICL`](#dc_icl-140c-invoke-new-cli)

[`DC_IN`](#dc_in-0e0c-read-from-cli) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_LCK`](#dc_lck-300c-display-lockup-box-and-ask-password) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_NAM`](#dc_nam-0c0c-name-current-process) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_NQ`](#dc_nq-160c-handle-directorcli-enquiries) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_OUT`](#dc_out-100c-write-to-cli) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_POL`](#dc_pol-220c-poll-for-card-or-application-usage)

[`DC_RBD`](#dc_rbd-1c0c-rebind-streams) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_RTE`](#dc_rte-240c-return-from-elf-program) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_SP`](#dc_sp-180c-handle-directorcli-settings) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_TTY`](#dc_tty-2c0c-ansi-terminal-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`DC_XIN`](#dc_xin-1e0c-examine-cli-input) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


[`FP`](#fp-calls-floating-point-package)


[`FP_ABS`](#fp_abs-51-magnitude-absolute-value) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_ACS`](#fp_acs-54-inverse-cosine-arc-cosine) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_ADD`](#fp_add-42-addition) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_AND`](#fp_and-21-bitwise-logical-and) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_ASN`](#fp_asn-57-inverse-sine-arc-sine) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_ATN`](#fp_atn-5a-inverse-tangent-arc-tangent)

[`FP_BAS`](#fp_bas-a2-call-floating-point-package-function) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_CMP`](#fp_cmp-9c-compare-two-numbers) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_COS`](#fp_cos-5d-cosine) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_DEG`](#fp_deg-60-convert-radians-to-degrees) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_DIV`](#fp_div-4e-division) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_EOR`](#fp_eor-27-bitwise-logical-xor)

[`FP_EQ`](#fp_eq-3c-test-for-equal-to) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_EXP`](#fp_exp-63-exponentiation-raise-e-2718-to-power-of) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_FIX`](#fp_fix-93-convert-to-integer-if-necessary) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_FLT`](#fp_flt-96-convert-to-float-if-necessary) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_GEQ`](#fp_geq-36-test-for-greater-than-or-equal-to) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_GT`](#fp_gt-45-test-for-greater-than)

[`FP_IDV`](#fp_idv-24-quotient-after-division) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_INT`](#fp_int-66-integer-truncation-floor-truncation-not-rounding) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_LEQ`](#fp_leq-30-test-for-less-than-or-equal-to) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_LN`](#fp_ln-69-natural-naperian-or-base-e-logarithm) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_LOG`](#fp_log-6c-common-base-10-logarithm) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_LT`](#fp_lt-39-test-for-less-than)

[`FP_MOD`](#fp_mod-2a-remainder-after-division) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_MUL`](#fp_mul-3f-multiplication) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_NEG`](#fp_neg-9f-negate-number) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_NEQ`](#fp_neq-33-test-for-not-equal-to) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_NOT`](#fp_not-6f-bitwise-logical-not) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_ONE`](#fp_one-84-one)

[`FP_OR`](#fp_or-2d-bitwise-logical-or) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_PI`](#fp_pi-8a-pi) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_PWR`](#fp_pwr-4b-raise-to-power) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_RAD`](#fp_rad-72-convert-degrees-to-radians) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_SGN`](#fp_sgn-72-return-sign--1-0-or-1) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_SIN`](#fp_sin-78-sine)

[`FP_SQR`](#fp_sqr-7b-square-root) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_STR`](#fp_str-90-return-the-string-representation-of-a-number) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_SUB`](#fp_sub-48-substraction) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_TAN`](#fp_tan-7e-tangent) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_TRU`](#fp_tru-87-true--1) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_TST`](#fp_tst-99-test-for-zero-and-sign)

[`FP_VAL`](#fp_val-8d-return-numeric-value-of-string) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FP_ZER`](#fp_zer-81-zero) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


[`GN`](#gn-calls-xx09-general)


[`GN_AAB`](#gn_aab-6809-allocate-alarm-block) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_ALP`](#gn_alp-7009-process-expired-alarm) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_CL`](#gn_cl-6209-close-file) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_CLS`](#gn_cls-3009-classify-character) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_CME`](#gn_cme-4209-compare-null-terminated-strings-one-local-one-extended) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_CPY`](#gn_cpy-8c09-copy-a-file)

[`GN_CRC`](#gn_crc-7c09-get-crc-32-of-ressource) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_D16`](#gn_d16-7409-16-bit-unsigned-division) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_D24`](#gn_d24-7809-24-bit-unsigned-division) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_DEI`](#gn_dei-1609-convert-zoned-external-format-date-to-internal-format) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_DEL`](#gn_del-6409-delete-filedirectory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_DIE`](#gn_die-1409-convert-internal-format-date-to-zoned-format)

[`GN_DIR`](#gn_dir-8809-create-directory-path) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_ELF`](#gn_elf-8209-enter-elf-program) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_ERR`](#gn_err-4a09-display-interactive-error-box) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_ESA`](#gn_esa-5e09-read-write-filename-segments) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_ESP`](#gn_esp-4c09-return-extended-pointer-to-system-error-message) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FAB`](#gn_fab-6a09-free-alarm-block)

[`GN_FCM`](#gn_fcm-4e09-compress-filename) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FEX`](#gn_fex-5009-expand-filename) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FLC`](#gn_flc-2409-close-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FLF`](#gn_flf-2a09-flush-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FLO`](#gn_flo-2209-open-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FLR`](#gn_flr-2809-read-character-from-filter)

[`GN_FLW`](#gn_flw-2609-write-character-to-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_FPB`](#gn_fpb-2c09-push-back-a-character-into-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_GAB`](#gn_gab-7e09-get-first-alarm-block) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_GDN`](#gn_gdn-1009-convert-ascii-string-to-integer) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_GDT`](#gn_gdt-0609-convert-ascii-string-to-internal-date) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_GHN`](#gn_ghn-8409-get-hexadecimal-number)

[`GN_GMD`](#gn_gmd-1809-get-machine-date-in-internal-format) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_GMT`](#gn_gmt-1a09-get-read-machine-system-time-in-internal-format) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_GTM`](#gn_gtm-0a09-convert-ascii-string-to-internal-time) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_LAB`](#gn_lab-6c09-link-alarm-into-alarm-list) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_LDM`](#gn_ldm-8009-localized-date-manipulation) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_LUT`](#gn_lut-8e09-lookup-table)

[`GN_LWR`](#gn_lwr-9409-to-lower) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_M16`](#gn_m16-7209-16-bit-unsigned-multiplication) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_M24`](#gn_m24-7609-24bit-unsigned-multiplication) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_MDT`](#gn_mdt-9009-get-the-current-machine-date-and-time-stamp-in-internal-format) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_MOV`](#gn_mov-8a09-move-file) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_MSC`](#gn_msc-2009-miscellaneous-time-operations)

[`GN_NLN`](#gn_nln-2e09-send-newline-to-stdout) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_OPF`](#gn_opf-6009-open-fileresource) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_OPW`](#gn_opw-5209-open-wildcard-handler) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PDN`](#gn_pdn-1209-write-integer-as-ascii) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PDT`](#gn_pdt-0809-convert-internal-date-to-ascii-string) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PFS`](#gn_pfs-5a09-parse-filename-segment)

[`GN_PHN`](#gn_phn-8609-put-hexadecimal-number) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PMD`](#gn_pmd-1c09-set-current-machine-date) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PMT`](#gn_pmt-1e09-set-current-machine-time) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PRS`](#gn_prs-5809-parse-filename) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_PTM`](#gn_ptm-0c09-convert-internal-time-to-ascii-string) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_RBE`](#gn_rbe-3e09-read-byte-at-extended-address)

[`GN_REN`](#gn_ren-6609-rename-filedirectory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_SDO`](#gn_sdo-0e09-send-date-and-time-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_SIP`](#gn_sip-oe09-system-input-line) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_SKC`](#gn_skc-3209-skip-character) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_SKD`](#gn_skd-3409-skip-delimiters-in-byte-sequence) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_SKT`](#gn_skt-3609-skip-to-value)

[`GN_SOE`](#gn_soe-3c09-write-string-at-extended-address-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_SOP`](#gn_sop-3a09-write-local-string-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_Swc`](#gn_swc-9609-swap-case) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_UAB`](#gn_uab-6e09-unlink-alarm-from-alarm-list) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_UPR`](#gn_upr-9209-to-upper) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_WBE`](#gn_wbe-4009-write-byte-at-extended-address)

[`GN_WCL`](#gn_wcl-5409-close-wildcard-handle) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_WFN`](#gn_wfn-5609-fetch-next-wildcard-match) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_WIN`](#gn_win-7a09-standard-window-create-with-banner-and-bottom-line) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_WSM`](#gn_wsm-5c09-match-filename-segment-to-wildcard-string) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_XDL`](#gn_xdl-4809-delete-an-entry-from-a-linked-list) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`GN_XIN`](#gn_xin-4609-insert-an-entry-into-a-linked-list)

[`GN_XNX`](#gn_xnx-4409-index-next-entry-in-linked-list) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


[`OS`](#os-calls-xx-xx03-xx06-operating-system)


[`OS_ALM`](#os_alm-81-low-level-alarm-manipulation) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_AXM`](#os_axm-c206-allocate-explicit-memory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_AXP`](#os_axp-d206-allocate-explicit-page) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BDE`](#os_bde-da06-copy-bytes-to-extended-address) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BHL`](#os_bhl-dc06-copy-bytes-from-extended-address) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BIX`](#os_bix-60-bind-in-extended-address)

[`OS_BLP`](#os_blp-d806-make-a-bleep) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BOUT`](#os_bout-90-write-null-terminated-string-block-at-bhl-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BOX`](#os_box-63-restore-bindings-after-os_bix) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BP`](#os_bp-ae03-break-point-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_BYE`](#os_bye-21-exit-current-application) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_CL`](#os_cl-e806-internal-close)

[`OS_CLI`](#os_cli-84-return-cli-status) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_DBG`](#os_dbg-b203-debugger-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_DEL`](#os_del-e606-delete-filedirectory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_DIS`](#os_dis-b003-disassemble-and-follow) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_DLY`](#os_dly-d606-delay-a-given-period) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_DOR`](#os_dor-87-dor-interface)

[`OS_ENT`](#os_ent-fa06-enter-an-application) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_EP`](#os_ep-b403-eprom-chip-low-level-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_EPR`](#os_epr-f006-eprom-filearea-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_ERC`](#os_erc-72-get-error-context) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_ERH`](#os_erh-75-set-error-handler) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_ESC`](#os_esc-6f-examine-special-condition)

[`OS_EXIT`](#os_exit-f606-quit-process-application) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FAT`](#os_fat-b603-sd-card-and-fat32-filesystem-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FC`](#os_fc-8a-select-fast-code-fast-bank-switching) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FDP`](#os_fdp-ba06-file-duplicate-sector-level) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FMA`](#os_fma-c406-find-memory-for-allocation) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FN`](#os_fn-7b-handle-functions)

[`OS_FRM`](#os_frm-48-file-read-miscellaneous) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FWM`](#os_fwm-4b-file-write-miscellaneous) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_FXM`](#os_fxm-c006-free-explicit-memory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_GB`](#os_gb-39-get-byte-from-stream) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_GBT`](#os_gbt-3f-get-byte-from-stream-with-timeout) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_HOUT`](#os_hout-96-write-hexadecimal-byte-to-standard-output)

[`OS_IN`](#os_in-2a-read-character-from-standard-input-without-timeout) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_ISO`](#os_iso-b806-key-to-iso-conversion) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_ISQ`](#os_isq-d006-initialize-prefix-sequence) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_KIN`](#os_kin-99-read-character-from-keyboard-with-timeout) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MAL`](#os_mal-54-allocate-memory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MAP`](#os_map-f406-pipedream-map-control)

[`OS_MCL`](#os_mcl-51-close-memory-free-memory-pool) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MFR`](#os_mfr-57-free-memory) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MGB`](#os_mgb-5a-get-current-bank-binding-in-segment) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MOP`](#os_mop-4e-open-memory-allocate-memory-pool) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MPB`](#os_mpb-5d-set-new-bank-binding-in-segment) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_MV`](#os_mv-45-move-bytes-between-stream-and-memory)

[`OS_NLN`](#os_nln-9f-write-crlf-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_NQ`](#os_nq-45-enquire) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_OFF`](#os_off-ec06-switch-machine-off) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_OP`](#os_op-ea06-internal-open) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_OUT`](#os_out-27-write-character-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_PB`](#os_pb-3c-write-byte-to-stream)

[`OS_PBT`](#os_pbt-42-write-byte-to-stream-with-timeout) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_PLOZ`](#os_ploz-c606-poll-for-oz-usage-in-slot) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_POLL`](#os_poll-fc06-poll-for-an-application) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_POUT`](#os_pout-93-write-embedded-null-terminated-string-to-standard-output) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_PRN`](#os_prn-ac03-send-character-directly-to-printer-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_PRT`](#os_prt-24-send-character-to-printer-filter)

[`OS_PUR`](#os_pur-33-purge-keyboard-buffer) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_RBE`](#os_rbe-a2-read-byte-at-extended-address) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_REN`](#os_ren-e406-file-rename) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_SCI`](#os_sci-d406-alter-screen-information) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_SI`](#os_si-8d-low-level-serial-interface) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_SOUT`](#os_sout-99-write-null-terminated-string-block-at-hl-to-standard-output)

[`OS_SP`](#os_sp-69-specify) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_SR`](#os_sr-6c-save-restore) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_STK`](#os_stk-f806-stack-file-current-process) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_TIN`](#os_tin-2d-read-character-from-standard-input-with-timeout) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_UASH`](#os_uash-be06-update-application-static-handle) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_USE`](#os_use-ee06-fetch-information-about-process-card-usage)

[`OS_UST`](#os_ust-78-update-small-timer) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_WAIT`](#os_wait-7e-snooze-until-system-event) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_WBE`](#os_wbe-a5-write-byte-at-extended-address) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_WRT`](#os_wrt-cc06-write-token) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_WSQ`](#os_wsq-ce06-write-to-prefix-sequence) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_WTB`](#os_wtb-ca06-write-token-base)

[`OS_WTS`](#os_wts-bc06-write-token-string) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OS_XIN`](#os_xin-30-examine-input) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


[`OZ`](#oz-calls-low-ram-vectors)


[`EXTCALL`](#extcall-rst-28-24bit-call-subroutine-in-external-bank) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`INT`](#int-0038-maskable-interrupt) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`NMI`](#nmi-0066-non-maskable-interrupt) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OZ_DI`](#call-oz_di-0051-disable-maskable-interruptions) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OZ_EI`](#call-oz_ei-0054-enable-maskable-interruptions) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OZ_MGB`](#call-oz_mgb-0057-memory-get-binding)

[`OZ_MPB`](#rst-oz_mpb-rst-30-memory-put-binding) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


[`X`](#appendix)


[`APPL_EXEC`](#application-execution) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`BAD_APPL`](#bad-and-ugly-applications) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`FILESYSTEM`](#oz-filesystem) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`HIRES`](#hires-font) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`ISO_FONT`](#iso-character-to-font-table) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`KEYMAP`](#keyboard-maps)

[`LORES`](#lores-font) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OSFRAME`](#osframe-os-stack-frame) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`OZ_WINDOW`](#oz-window) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[`PRINTER`](#printer-filter) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

---

# DC calls ($XX0C) : director and CLI
```

       This API refers to Index (a.k.a. the director), process and CLI calls.
       It contains the low level implementation of ELF standard 
       including environment variables. Moreover, the lockup box and 
       the terminal device :TTY have been added.

```
## DC_Alt ($1A0C) : pass an alternative character
```

 IN:   A = char

 OUT:  -

 DIF:  AF....../..../afbcdehl

 NOTE: handles keys after []
       this call manages redirection []+Key / []-Key
       and application launch or revival

```
## DC_Bye ($080C) : exiting current application
```

 IN:   A = return code

 OUT:  -

 NOTE: DC_Bye returns to latest process or Index if none

```
## DC_ELF ($260C) : ELF low level interface
```

 IN:   A = reason call

 OUT:  depends on reason call

```
### EL_CL ($0A) : Close ELF file
```

 IN :  IX = ELF handle

 OUT:  Fc = 0, success, IX = ELF file handle
       Fc = 1, A = error

```
### EL_EX ($08) : Get execution entry
```

 IN:   IX = ELF handle
       B  = execution mode (B0=0:execute,  B0=1:debug)
 OUT:  -

```
### EL_HI ($02) : Get ELF header information
```

 IN:   IX = ELF handle

 OUT:  success, Fc = 0 and
               A  = ELF type
               BC = number of PHT
               DE = number of SHT
               HL = ELF header buffer address
       failure, Fc = 1 and A = error

 DIF:  AFBCDEHL/....

```
### EL_LD ($06) : Load ELF segment to memory
```

 IN :  IX = ELF handle
       C  = program header table index

 OUT:  success, Fc = 0 program loaded and
               A = bank allocated
               BC = program address allocated
               DE = memory size allocated
               HL = PHT buffer address
       failure, Fc = 1 and A = error

 DIF:  AFBCDEHL/....

```
### EL_OP ($00) : Open ELF file
```

 IN:   IX = file handle

 OUT:  success, Fc = 0 and IX = ELF handle
               A  = ELF type
               HL = number of program header
               DE = number of section header
               BC = section name string index
       failure, Fc = 1 and A  = error

 DIF:  AFBCDEHL/IX..

```
### EL_PI ($04) : Get program header information for memory allocation
```

 IN:   IX = ELF handle
       C  = program header index

 OUT:  A  = program type
       BC = program address requested
       DE = memory size requested
       HL = PHT buffer address

 DIF:  AFBCDEHL/....

```
## DC_Env ($2A0C) : system variable environment API
```

 IN:   A = reason call
       registers depend on reason call detailed below

 NOTE:
       About Environment API
       ---------------------

       This API has been developped to manage environment data attached to a 
       process.

       It intends to store common variables like process name, default device,
       default directory, default name match string.
       Moreover, it allows to store useful variables for shell usage like
       include path, display options...

       Variable scope is limited to the process (except Index). All variables
       attached to the process are destoyed when the process is killed.

       Index variables are defined as global and never dies. They are only
       accessed at system level.
       They contains system variables like default device (DEV), default
       directory (DIR) and filename match string (FNM) and are accessed with
       dedicated NQ/SP calls. They're useful to pass current path between
       shell and Filer.

       When defined, process name is saved in a local environment variable
       labelled (NAM).

       Environment variable is written in a memory chunk (up to 255 bytes).
       Null terminated name and null terminated data are stored 
       after link pointer and size of chunk

       Name is case insensitive.
       Data string should not contain space. If any, it is treated as a null
       terminator. As does the control characters.

       The first block is pointed to by the process block.

       Environment block Structure
       ---------------------------

               ds.p    link (null if none)
               ds.b    size of chunk
                       name.0 (up to 32 bytes null included)
                       data.0 (up to 220 bytes null included)

       memory chunk allocation is performed in the DC memory pool
       used by Index to save process
       
```
### EV_EXP ($0C), expand environment variable in a string
```

 IN:   BHL = pointer to buffer containing string to be expanded
       buffer size at least 256 bytes are required

 OUT:  Fc = 0, always, A = length of expanded string (including null terminator)

 DIF:  AFBCDEHL/..../afbcdehl
 API:  AF....../..../afbcdehl

```
### EV_GET ($04), get environment variable
```

 IN:   DE  = pointer to name (null terminated)

 OUT:  Fc = 0, success, BHL = pointer to data (null terminated)
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
### EV_IDX ($06), get first global environment variable pointer
```

 IN:   -

 OUT:  Fc = 0, success
               BHL = pointer to first variable name (null terminated)
               BDE = pointer to data (null terminated)

       Fc = 1, A = RC_Onf, no global environment

 DIF:  AFBCDEHL/..../afbcdehl

```
### EV_LOC ($08), get first local environment variable pointer
```

 IN:   -

 OUT:  Fc = 0, success
               BHL = pointer to first variable name (null terminated)
               BDE = pointer to data (null terminated)

       Fc = 1, A = RC_Onf, no local environment

 DIF:  AFBCDEHL/..../afbcdehl

```
### EV_NXT ($0A), get next environment variable pointer
```

 IN:   BHL = pointer to variable name

 OUT:  Fc = 0, success
               BHL = pointer to next variable name (null terminated)
               BDE = pointer to data (null terminated)

       Fc = 1, A = RC_Eof, no more variable

 DIF:  AFBCDEHL/..../afbcdehl

```
### EV_SET ($00), set environment variable
```

 IN:   DE  = pointer to name (null terminated)
       BHL = pointer to data (null terminated)

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
### EV_UNS ($02), unset (delete) environment variable
```

 IN:   DE  = pointer to name (null terminated)

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
## DC_Gen ($200C) : screen driver SOH call, toggle redirection to printer
```

 IN:   A = reason ('[' or ']')

 OUT:  Fc = 0, always

 DIF:  AF....../....

 NOTE: if '[' or ']' it enables or disables redirection to printer
       it avoids sending SOH sequences to the printer

```
## DC_Icl ($140C) : invoke new CLI
```

 IN:   BHL = string, null terminated, B = 0 local string
       C   = length of string
       IX  = input stream if BHL = 0

 OUT:  Fc = 0, success
       Fc = 1, error, A = RC_Room, RC_Fail (if :INP stream)

 DIF:  AF....../....

```
## DC_In ($0E0C) : read from CLI
```

 IN:   BC = timeout

 OUT:  Fc = 0, DE = CLI_InStreamT handle, BC = remaining timeout
       Fc = 1, error in A (RC_Eof, RC_Time, RC_Susp, RC_Esc)

 DIF:  AFBCDE../....

 NOTE: 
       CLI commands
       ------------

       .< INFILE       take input from file/device "INFILE" 
       .> OUTFILE      send output to file/device "OUTFILE"

       .T< INFILE      send copy of input to file/device "INFILE" 
       .T> OUTFILE     send copy of output to file/device "OUTFILE"

       .= PRTFILE      redirect pre-filter printer output to file/device
       .T= PRTFILE     as .=, but also send the output to the printer filter
       
       .* FILE         invoke a new CLI file
       .S              suspend the current CLI but maintain all rebindings
       .J              jammer, ignore all special sequences for the rest of the CLI
       .;              comment, rest of line until CR is ignore by CLI
       .D              delay for n centiseconds

       |               press and hold ctrl
       ||              |
       #               press and hold alt
       ##              #
       ~~              ~
       ~A              alt
       ~C              ctrl
       ~D              down
       ~E              enter
       ~H              help
       ~I              index
       ~L              left
       ~M              menu
       ~R              right
       ~S              exit
       ~T              tab
       ~U              up
       ~X              del

```
## DC_Lck ($300C) : display lockup box and ask password
```

 IN:   -

 OUT:  Fc = 1, always, A = return code
               RC_SUSP, screen preserved, return pre-emption
               RC_DRAW, screen corrupted

 DIF:  AF....../..../afbcdehl

 NOTE: password is stored in preserved low ram
       thus, only a hard reset can recover a lost password

```
## DC_Nam ($0C0C) : name current process
```

 IN:   HL = pointer to a null terminated name

 OUT:  -

 DIF:  .F....../....

 NOTE: call rewritten in OZ5.0
       process name is now stored as an environment variable

```
## DC_Nq ($160C) : handle Director/CLI enquiries
```

 IN:   A = C = reason code
       B = 0, always
       HL = OSFrame (from OS_Nq)

 OUT:  Fc = 0, success, result in IX or BHL according reason
       Fc = 1, error, A = RC_Unk

 NOTE: system use only
       reason calls must be accessed by OS_Nq

```
### NQ_CHN ($8C1E) : reset serial port and get NQ_Com
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = serial port handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_DEV ($8C00) : fetch current device
```

 IN:   BC = reason

 OUT:  Fc = 0, always, BHL = pointer to process

 DIF:  .FB...HL/..../afbcdehl

```
### NQ_DIR ($8C03) : fetch current directory
```

 IN:   BC = reason

 OUT:  Fc = 0, always, BHL = pointer to process

 DIF:  .FB...HL/..../afbcdehl

```
### NQ_DMH ($8C09) : fetch director memory handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = director memory handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_FMH ($8C24) : fetch filesystem memory handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = filesystem memory handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_FNM ($8C06) : fetch current filename match string
```

 IN:   BC = reason

 OUT:  Fc = 0, always, BHL = pointer to process

 DIF:  .FB...HL/..../afbcdehl

```
### NQ_INP ($8C0C) : read std input handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = input handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_NAM ($8C27) : fetch process name
```

 IN:   BC = reason
       A  = Pid, if A = 0, current Pid is used

 OUT:  Fc = 0, success, BHL = pointer to process
       Fc = 1, failure, A = error

 DIF:  AFB...HL/..../afbcdehl

```
### NQ_OUT ($8C0F) : read std output handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = output handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_PRC ($8C21) : fetch process environment
```

 IN:   BC = reason
       A  = Pid, if A = 0, current Pid is used

 OUT:  Fc = 0, success, BHL = pointer to process
       Fc = 1, failure, A = error

 DIF:  AFB...HL/..../afbcdehl

```
### NQ_PRT ($8C12) : read printer stream handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = printer handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_TIN ($8C15) : read input-T handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = input-T handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_TOT ($8C18) : read output-T handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = output-T handle

 DIF:  .F....../IX../afbcdehl

```
### NQ_TPR ($8C1B) : read printer-T handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = printer-T handle

 DIF:  .F....../IX../afbcdehl

```
## DC_Out ($100C) : write to CLI
```

 IN:   A = character to output

 OUT:  Fc = 0, always

 DIF:  .F....../....

```
## DC_POL ($220C) : poll for card or application usage
```

 IN:   A = card slot (0 to 3), 0 is internal / A7=1 test IX application handle

 OUT:  Fz = 1 if slot not in use
       Fz = 0 if slot in use

 DIF:  .F....../....

```
## DC_Rbd ($1C0C) : rebind streams
```

 IN:   A  = identifier for stream to rebind
       IX = new stream

 OUT:  Fc = 0, success
       Fc = 1, error in A (RC_Fail or RC_Bad)

 DIF:  AF....../....

```
## DC_RTE ($240C) : Return from ELF program
```

 IN:   IX = AF from ELF (F is destroyed by call entry)

 OUT:  AFBCDEHL from ELF program

```
## DC_Sp ($180C) : handle Director/CLI settings
```

 IN:   A = C = reason code
       B = 0, always
       HL = OSFrame (from OS_Sp)

 OUT:  Fc = 0, success
       Fc = 1, error in A

 NOTE: system use only
       reason calls must be accessed by OS_Sp

```
### SP_DEV ($8C00) : set current device
```

 IN:   BC = reason
       HL = device string

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

 NOTE: if string is null, default device (PA_Dev) is set

```
### SP_DIR ($8C03) : set current directory
```

 IN:   BC = reason
       HL = directory path string

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

 NOTE: use always a '/' in front of the path name

```
### SP_FNM ($8C06) : set filename match string
```

 IN:   BC = reason
       HL = filename match string

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## DC_Tty ($2C0C) : ANSI terminal output
```

 IN:   A = character to output

 OUT:  Fc = 0, success
       Fc = 1, error

 DIF:  .F....../....

```
## DC_Xin ($1E0C) : examine CLI input
```

 IN:   -

 OUT:  Fc = 0, CLI has input stream
       Fc = 1, A = RC_Eof, there is no CLI or no input stream

 DIF:  AF....../....

```
# FP calls : Floating Point Package
```

       This API implements 32bit integer and 40bit float maths.
       Written by R.T. Russell, it was imported from BBC Basic Z80 V3.0 (CP/M).
       Version 0.0 was released the 26/10/1988.
       It is updated with the latest bugfix (14/12/1988, Version 0.1).

       Binary floating point representation :
               32 bit sign-magnitude normalized mantissa
                8 bit excess-128 signed exponent
               sign bit replaces mantissa msb (implied "1")
               mantissa=0 & exponent=0 implies value is zero.
       
       Binary integer representation:
               32 bit 2's-complement signed integer
               "exponent" byte = 0 (when present)
       
       Normal register allocation    : mantissa - HLhl
                                       exponent - C
       Alternate register allocation : mantissa - DEde
                                       exponent - B

```
## FP_ABS ($51) : magnitude (absolute value)
```

 IN:   HLhlC

 OUT:  Fc = 0, always, HLhlC = ABS(HLhlC)

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_ABS (16) for FP_BAS

```
## FP_ACS ($54) : inverse cosine (Arc CoSine)
```

 IN:   HLhlC

 OUT:  Fc = 0, result in HLhlC = ACS(HLhlC)
               result is a float in radians

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_ACS (17) for FP_BAS

```
## FP_ADD ($42) : addition
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, always, HLhlC = HLhlC + DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_ADD (11) for FP_BAS

```
## FP_AND ($21) : bitwise logical AND
```

 IN:   HLhlC
       DEdeB
       integer only (BC = 0)

 OUT:  Fc = 0, always, HLhlC = HLhlC & DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_AND (0) for FP_BAS

```
## FP_ASN ($57) : inverse sine (Arc SiNe)
```

 IN:   HLhlC

 OUT:  Fc = 0, result in HLhlC = ASN(HLhlC)
               result is a float in radians

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_EXP (18) for FP_BAS

```
## FP_ATN ($5A) : inverse tangent (Arc TaNgent)
```

 IN:   HLhlC

 OUT:  Fc = 0, result in HLhlC = ATN(HLhlC)
               result is a float in radians

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_EXP (19) for FP_BAS

```
## FP_BAS ($A2) : call Floating point package function
```

 IN:   A = function (FB_xx)

 OUT:  depends on reason call

 DIF:  depends on reason call

```
## FP_CMP ($9C) : compare two numbers
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, always
               A = $00, if equal
               A = $40, if HLhlC > DEdeB
               A = $C0, if HLhlC < DEdeB

 DIF:  AF....../..../afbc....

 NOTE: FB_CMP (42) for FP_BAS

```
## FP_COS ($5D) : cosine
```

 IN:   HLhlC in radians

 OUT:  Fc = 0, success, float in HLhlC
       Fc = 1, failure, A = RC_Acl, accuracy lost

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: FB_COS (20) for FP_BAS

```
## FP_DEG ($60) : convert radians to degrees
```

 IN:   HLhlC

 OUT:  Fc = 0, always, float in HLhlC

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_DEG (21) for FP_BAS
       result is a float

```
## FP_DIV ($4E) : division
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, always, HLhlC = HLhlC / DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_DIV (15) for FP_BAS

```
## FP_EOR ($27) : bitwise logical XOR
```

 IN:   HLhlC
       DEdeB
       integer only (BC = 0)

 OUT:  Fc = 0, always, HLhlC = HLhlC ^ DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_EOR (2) for FP_BAS

```
## FP_EQ ($3C) : test for equal to
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, test (HLhlC > DEdeB) 
       result in HLhlC : -1 if true, 0 if false

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_EQ (9) for FP_BAS

```
## FP_EXP ($63) : exponentiation, raise e (2.718...) to power of
```

 IN:   HLhlC

 OUT:  Fc = 0, always, result in HLhlC = e ^ HLhlC

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_EXP (22) for FP_BAS
       result is a float

```
## FP_FIX ($93) : convert to integer if necessary
```

 IN:   HLhlC

 OUT:  Fc = 0, always, integer in HLhl, C = 0

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_FIX (38) for FP_BAS

```
## FP_FLT ($96) : convert to float if necessary
```

 IN:   HLhlC

 OUT:  Fc = 0, always, HLhlC is a float (C<>0)

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_FLT (39) for FP_BAS

```
## FP_GEQ ($36) : test for greater than or equal to
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, test (HLhlC > DEdeB) 
       result in HLhlC : -1 if true, 0 if false

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_GEQ (7) for FP_BAS

```
## FP_GT ($45) : test for greater than
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, test (HLhlC > DEdeB) 
       result in HLhlC : -1 if true, 0 if false

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_GT (12) for FP_BAS

```
## FP_IDV ($24) : quotient after division
```

 IN:   HLhlC
       DEdeB
       integer only (BC = 0)

 OUT:  Fc = 0, always, HLhlC = HLhlC / DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_IDV (1) for FP_BAS

```
## FP_INT ($66) : integer truncation (floor truncation, not rounding)
```

 IN:   HLhlC

 OUT:  Fc = 0, always, integer in HLhlC (C=0)

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_INT (23) for FP_BAS

```
## FP_LEQ ($30) : test for less than or equal to
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, test (HLhlC > DEdeB) 
       result in HLhlC : -1 if true, 0 if false

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_LEQ (5) for FP_BAS

```
## FP_LN ($69) : natural (Naperian or base e) logarithm
```

 IN:   HLhlC

 OUT:  Fc = 0, always, result in HLhlC = ln(HLhlC)

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_LN (24) for FP_BAS
       result is a float

```
## FP_LOG ($6C) : common (base 10) logarithm
```

 IN:   HLhlC

 OUT:  Fc = 0, always, result in HLhlC = log(HLhlC)

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_LOG (25) for FP_BAS
       result is a float

```
## FP_LT ($39) : test for less than
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, test (HLhlC < DEdeB) 
       result in HLhlC : -1 if true, 0 if false

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_LT (8) for FP_BAS

```
## FP_MOD ($2A) : remainder after division
```

 IN:   HLhlC
       DEdeB
       integer only (BC = 0)

 OUT:  Fc = 0, always, HLhlC = HLhlC % DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_MOD (3) for FP_BAS

```
## FP_MUL ($3F) : multiplication
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, always, HLhlC = HLhlC * DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_MUL (10) for FP_BAS

```
## FP_NEG ($9F) : negate number
```

 IN:   HLhlC

 OUT:  Fc = 0, always, HLhlC = -HLhlC

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_NEG (42) for FP_BAS

```
## FP_NEQ ($33) : test for not equal to
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, test (HLhlC > DEdeB) 
       result in HLhlC : -1 if true, 0 if false

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_NEQ (6) for FP_BAS

```
## FP_NOT ($6F) : bitwise logical NOT
```

 IN:   HLhlC
       integer only (C=0)

 OUT:  Fc = 0, always, HLhlC = !HLhlC

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_NOT (26) for FP_BAS

```
## FP_ONE ($84) : one
```

 IN:   -

 OUT:  Fc = 0, always, float in HLhlC = $0000000080

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_ONE (33) for FP_BAS
       result is a float

```
## FP_OR ($2D) : bitwise logical OR
```

 IN:   HLhlC
       DEdeB
       integer only (BC = 0)

 OUT:  Fc = 0, always, HLhlC = HLhlC | DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_OR (4) for FP_BAS

```
## FP_PI ($8A) : pi
```

 IN:   -

 OUT:  Fc = 0, always, HLhlC = $490FDAA281

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_PI (35) for FP_BAS
       result is a float

```
## FP_PWR ($4B) : raise to power
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, always, HLhlC = HLhlC ^ DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_PWR (14) for FP_BAS

```
## FP_RAD ($72) : convert degrees to radians
```

 IN:   HLhlC

 OUT:  Fc = 0, always, float in HLhlC

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_RAD (27) for FP_BAS
       result is a float

```
## FP_SGN ($72) : return sign (-1, 0 or +1)
```

 IN:   HLhlC

 OUT:  Fc = 0, always, integer in HLhlC (C=0)
                       -1 if neative
                        0 if zero
                       +1 if positive

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_SGN (28) for FP_BAS
       result is an integer

```
## FP_SIN ($78) : sine
```

 IN:   HLhlC in radians

 OUT:  Fc = 0, success, float in HLhlC
       Fc = 1, failure, A = RC_Acl, accuracy lost

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: FB_SIN (29) for FP_BAS

```
## FP_SQR ($7B) : square root
```

 IN:   HLhlC

 OUT:  Fc = 0, success, float in HLhlC
       Fc = 1, failure, A = RC_Nvr, negative root

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_SQR (30) for FP_BAS

```
## FP_STR ($90) : return the string representation of a number
```

 IN:   HLhlC = number
       DE = string buffer
       de = format control vartiable (@% in BBC Basic)

 OUT:  Fc = 0, DE points to character after string 

 DIF:  AF.CDEHL/..../afbc..hl

 NOTE: FB_STR (37) for FP_BAS

       Format Control Variable (de)
       ----------------------------

       1) The contents of e determine the maximum number of
       digits (ie. number of characters apart from decimal
       point or 'E') to be printed. The allowed range of
       values depends on the format selected (see below):
       
       General format
       Exponential format: 1-255 (leading zeros added beyond
       the 10th significant figure).
       Fixed format: 0-10 (in this case the number of digits
       printed after the decimal point).
       
       2) The contents of d determine the format of the
       resultant string for a given number; the options are:
       
       d=0: General format
       Integers are printed without a decimal point or
       exponent. Numbers between 0.1 and 1 will be printed
       with a decimal point but no exponent. Numbers less than
       0.1 will be printed with a decimal point and exponent.
       Numbers greater than 1 will be printed without exponent
       unless this would involve more digits than allowed by
       the contents of the 'e' register (se above), and
       outside this range, will be printed with an exponent.
       
       d=1: Exponential format
       All numbers printed in exponential notation, eg. 1.0E0.
       
       d=2: Fixed format
       All numbers are printed with a fixed number of decimal
       places.
       
```
## FP_SUB ($48) : substraction
```

 IN:   HLhlC
       DEdeB

 OUT:  Fc = 0, always, HLhlC = HLhlC - DEdeB

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_SUB (13) for FP_BAS

```
## FP_TAN ($7E) : tangent
```

 IN:   HLhlC in radians

 OUT:  Fc = 0, success, float in HLhlC
       Fc = 1, failure, A = RC_Tbg, overflow, A = RC_Dvz, division by zero

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: FB_TAN (31) for FP_BAS

```
## FP_TRU ($87) : true (-1)
```

 IN:   -

 OUT:  Fc = 0, always, HLhlC = $FFFFFFFF00

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_TRU (34) for FP_BAS
       result is an integer

```
## FP_TST ($99) : test for zero and sign
```

 IN:   HLhlC

 OUT:  Fc = 0, always
               A = $00, if zero
               A = $40, if positive
               A = $C0, if negative

 DIF:  AF....../..../afbc....

 NOTE: FB_TST (40) for FP_BAS

```
## FP_VAL ($8D) : return numeric value of string
```

 IN:   HL = pointer to a null terminated string

 OUT:  Fc = 0, success, integer or float in HLhlC, DE points to null terminator
       Fc = 1, failure, A = RC_Bdn, Bad number

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: FB_VAL (36) for FP_BAS

```
## FP_ZER ($81) : zero
```

 IN:   -

 OUT:  Fc = 0, always, HLhlC = $0000000000

 DIF:  AF.C..HL/..../afbc..hl

 NOTE: FB_ZER (32) for FP_BAS
       result is an integer

```
# GN calls ($xx09) : general
```

       This API implements hi-level calls and covers various area :
       file i/o, alarms, date and time, integers, strings...

```
## GN_Aab ($6809) : allocate alarm block
```

 IN:   -

 OUT:  Fc = 0, success, BHL = alarm
       Fc = 1, failure, A = error

 DIF:  AFB...HL/..../afbcdehl

```
## GN_Alp ($7009) : process expired alarm
```

 IN:   IX = alarm ID of alarm just expired

 OUT:  Fc = 0, always

 DIF:  .F....../..../afbcdehl

```
## GN_Cl ($6209) : close file
```

 IN:   IX = handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, failure, A  = error

 DIF:  AF....../IX../afbcdehl

```
## GN_Cls ($3009) : classify character
```

 IN:
      A = character to classify

 OUT:
      F = flags indicate classification, as follows:
           Fc = 0, Fz = 0 : Neither alphabetic or numeric
           Fc = 0, Fz = 1 : Numeric ('0' ... '9')
           Fc = 1, Fz = 0 : Upper case letter ('A' ... 'Z')
           Fc = 1, Fz = 1 : Lower case letter ('a' ... 'z')

 DIF:  AF....../..../afbcdehl

 NOTE:
       OZ 5.0 rewrite with full ISO Latin 9 support

       About ISO 8859-15 (Latin 9) support
       -----------------------------------

       API providing ASCII and ISO character manipulation.
       ISO alphabetic character case can not be swapped by an 'XOR $20',
       ToUpper can not be 'AND $DF' and ToLower an 'OR $20' as for ASCII.
       Therefore the API below :

               GN_Cls : classify character
               GN_Upr : to upper case
               GN_Lwr : to lower case
               GN_Swc : swap case
       

```
## GN_Cme ($4209) : compare null-terminated strings, one local, one extended
```

       comparison is case insensitive, "aaa" == "AAA"

 IN:   BHL = string1
       DE  = string2

 OUT:  Fz = 1, strings are the same
       Fz = 0, strings are different

 DIF:  .F....../..../afbcdehl

 NOTE: this call comply with ISO Latin 9

```
## GN_Cpy ($8C09) : copy a file
```

 IN:   A = copy reason (CP_TODIR or CP_TOFILE)
       IX, BHL, CDE = file arguments, based on action

 OUT:  Fc = 0, success, file copied to destination

    Fc = 1, failure, A = error
         A = RC_ONF, File Object was not found
         A = RC_xxx, I/O error, No Room during saving process.

 DIF:  AFBCDEHL/..../afbcdehl

```
## GN_Crc ($7C09) : get CRC-32 of ressource
```

 IN:   A = reason call
               CRC_MEMA ($00)
               CRC_FILE ($01)
               CRC_PARTFILEBYTE ($02)
               CRC_PARTFILEBUF ($03)
               CRC_EPFILE ($04)
               CRC_PARTEPFILE ($05)

 OUT:  depends on reason call

```
## GN_D16 ($7409) : 16-bit unsigned division
```

 IN:   HL = divident
       DE = divisor

 OUT:  Fc = 0, success
               HL = quotient
               DE = remainder
       Fc = 1, A = RC_Dvz if divide by zero

 DIF:  AF..DEHL/..../afbcdehl

```
## GN_D24 ($7809) : 24-bit unsigned division
```

 IN:   BHL = divident, CDE = divisor

 OUT:  Fc = 0, success
               BHL = quotient
               CDE = remainder
       Fc = 1, A = RC_Dvz if divide by zero

 DIF:  AFBCDEHL/..../afbcdehl

```
## GN_Dei ($1609) : convert zoned, external format date to internal format
```

 IN:   B = month
       C7-C5 = weekday (0=unspecified)
       C4-C0 = day of month
       DE = year

 OUT:  ABC = internal date

 DIF:  AFBC..../....

```
## GN_Del ($6409) : delete file/directory
```

 IN:   BHL = filename

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## GN_Die ($1409) : convert internal format date to zoned format
```

 IN:   ABC = internal date

 OUT:  A = number of days in month
       B = month (1 = Jan, 12 = Dec)
       C7-C5 = day of week (1=Mon, 7=Sun)
       C4-C0 = day of month (1-31)
       DE = year

 DIF:  AFBCDE../....

```
## GN_Dir ($8809) : create directory path
```

 IN:   BHL = pointer to null-terminated directory path name

 OUT:  Fc = 0, directory successfully created
       Fc = 1, unable to create directory
         A = error code:
              RC_IVF, Invalid directory path name
              RC_USE, Directory in use
              RC_EXIS, Directory already exists

 DIF:  .F....../..../afbcdehl

 NOTE:
       The directory name must not contain wildcards (standard convention).
       However if a RAM device is not specified, it will automatically be included
       (the current) into the directory name.

       The buffer of the directory name must have space enough to get itself expanded
       with a device name (additional 6 bytes).

```
## GN_Elf ($8209) : enter Elf program
```

 IN :  BHL = explicit filename (null terminated)
       CDE = optional arguments line (space separated, null terminated)
       A   = execution mode (A0=0: execute, A0=1:debug)

 OUT:  success, Fc = 0 and AFBCDEHL depends on ELF command executed
       failure, Fc = 1 and A = error

 NOTE: implemented in OZ5.0

```
## GN_Err ($4A09) : display interactive error box
```

 IN:   A =  0, display error message at BHL and C = 0 if fatal (B = 0, local)
       A <> 0, display system error message of error code A
       
 OUT:  Fc = 1, A = error
               RC_SUSP: error not fatal
               RC_DRAW: error not fatal, windows corrupted
               RC_QUIT: error fatal

 DIF:  AF....../..../afbcdehl

```
## GN_Esa ($5E09) : read & write filename segments
```

 IN:   A = command (A7=0: read, A7=1: write, A0=0: name, A1=1: extension)
       B = segment number (+/-64)
       read:  HL = filename, DE = buffer
       write: HL = segment,  DE = buffer, C = buffer size

 OUT:  Fc = 0, success
       read:  C = #chars, DE = buffer end (if DE>255)
       write: B = #segments, C = #chars, DE = buffer end (if DE>255) or DE(in)-1 (DE<256)
       Fc = 1, failure, A = error

 DIF:  read:   AF.CDE../..../afbcdehl
       wwrite: AFBCDE../..../afbcdehl

```
## GN_Esp ($4C09) : return extended pointer to system error message
```

 IN:   A = error code
 OUT:  BHL = error message
       Fz = 1, error is fatal
       Fz = 0, error is NOT fatal

 DIF:  .FB...HL/..../afbcdegl

```
## GN_Fab ($6A09) : free alarm block
```

 IN:   BHL = alarm

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## GN_Fcm ($4E09) : compress filename
```

 IN:   BHL = source
       DE  = destination
       C   = dest size
       IX  = destination handle (if DE<2)

 OUT:  Fc = 0, success
               DE  = destination end (if DE>255)
               B   = number of segments written
               C   = number of chars written
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

```
## GN_Fex ($5009) : expand filename
```

 IN:   BHL = source
       DE  = destination
       C   = dest size
       IX  = destination handle (if DE<2)

 OUT:  Fc = 0, success
               DE = destination end (if DE>255)
               B  = number of segments written
               C  = number of chars written
               A  = flags
                       A0: extension specified
                       A1: filename specified
                       A2: explicit directory specified
                       A3: current directory (".") specified
                       A4: parent directory ("..") specified
                       A5: wild directory ("//") specified
                       A6: device name specified
                       A7: wildcards were used
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

```
## GN_Flc ($2409) : close filter
```

 IN:   IX = filter handle

 OUT:  Fc = 0, success
               BC = number of characters written into filter
               DE = number of characters read from filter
       Fc = 1, failure, A = error

 DIF:  AFBCDE../IX..

```
## GN_Flf ($2A09) : flush filter
```

 IN:   IX = filter

 OUT:  Fc = 0, success, A = char, Fz = 1 if converted
       Fc = 1, failure, A = error

 DIF:  AF....../....

```
## GN_Flo ($2209) : open filter
```

IN:    HL = filter table
       B  = max buffer size (if A2 = 1)
       A  = flags
          A0 = ignore case
          A1 = reverse mode
          A2 = force max buffer size B

 OUT: IX = filter

 DIF:  AF....../IX../afbcdehl

 NOTE: HL can point anywhere in the memory space (OZ 5.0 and newer)

```
## GN_Flr ($2809) : read character from filter
```

 IN:   IX = filter

 OUT:  Fc = 0, success, A = char, Fz = 1 if character is converted
       Fc = 1, failure, A = error

 DIF:  AF....../....

```
## GN_Flw ($2609) : write character to filter
```

 IN:   A  = char
       IX = filter

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../....

```
## GN_Fpb ($2C09) : push back a character into filter
```

 IN:   IX = filter

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../....

```
## GN_Gab ($7E09) : get first alarm block
```

 IN:   -

 OUT:  Fc = 0, success, BHL = first alarm block in list
       Fc = 1, failure, A = RC_Eof, no existing alarm list

 DIF:  AFB..HL/..../afbcdehl

```
## GN_Gdn ($1009) : convert ASCII string to integer
```

 IN:   HL = source
               HL = 0, read from stream IX
               HL = 1, read from filter IX
               HL = 2, not allowed
               HL > 255, read string at (HL)
       DE = destination
               DE = 2, return result in BC
               DE > 255, store integer at (DE)
       IX = source handle (if HL<2)
       B  = max chars input

 OUT:  BC = integer (if DE<256)
       HL = source index/pointer
       DE = destination pointer

```
## GN_Gdt ($0609) : convert ASCII string to internal date
```

 IN:   HL = source
       DE = destination
       IX = source handle (if HL<2)
       A  = format
       B  = max chars in
       C  = delimter (if A5=1)

 OUT:  Fc = 0, success
               ABC = date
               HL  = input index/ptr
       Fc = 1, failure, A = error

 DIF:  ABC..HL/..../afbcdehl

```
## GN_Ghn ($8409) : get hexadecimal number
```

       convert hexadecimal ascii string to 32 bits integer

 IN :  HL = local source pointer
       DE = 2, return integer in DEBC
       DE > 255, return integer at (DE) and (DE)=LSB, (DE+3)=MSB

 OUT:  Fc = 0, success
       Fc = 1, A = RC_Bdn (bad hexadecimal string)
               A = RC_Fail (bad destination)
               A = RC_Hnd (bad handle for stream IX or filter IX)

 DIF:  .F....../..../afbcdehl

 NOTE: implemented in OZ5.0, based on ConvHex from G. Strube

```
## GN_Gmd ($1809) : get machine date in internal format
```

 IN:   D = 0, destination to (DE)
       D <>0, destination to ABC

 OUT:  Fc = 0, always
               ABC  = date (if DE<256)
               (DE) = date (if DE>255) and DE = DE + 3 

 DIF:  AFBCD../..../afbcdehl

```
## GN_Gmt ($1A09) : get (read) machine system time in internal format
```

 IN:   C  = least significant byte from GN_Gmd (optional)
       DE = destination

 OUT:  ABC  = time (if DE<256)
       (DE) = time (if DE>255) and DE=DE + 3
       Fz   = 1, if C(in) is consistent with time

 DIF:  AFBCDE../..../afbcdehl

 NOTE: time consistency means that 
       GN_Gmt following GN_Gmd have the same centiseconds count
       C(in) is the centiseconds count returned by GN_Gmd

```
## GN_Gtm ($0A09) : convert ASCII string to internal time
```

 IN:   HL = source
       DE = destination
       IX = source handle (if HL<2)

 OUT:  Fc = 0, success, 
               ABC = time (if DE=2)
               HL  = input pointer
       Fc = 1, failure, A = error

 DIF:  AFBC..HL/..../afbcdehl

```
## GN_Lab ($6C09) : link alarm into alarm list
```

 IN:   BHL = alarm

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## GN_Ldm ($8009) : Localized Date Manipulation
```

       Get country localized string (full or short) of day, month or moment

 IN :  A = desired string
       A7 = 1  get day of the week
       A6 = 1  get month name
       A5 = 0  full string, 1 short string
       A4 = 1  get moment (no short string available)
       A0-A3   day of the week from 1-7 (Monday-Sunday)
        or     month name 1-12 (January_December)
        or     moment : 1=today, 2=yesterday, 3=tomorrow
 
       HL = Destination buffer (at least 16 bytes required)

 OUT:  Fc = 0, HL = null terminated string desired
               DE = zero at the end of the string
                B = number of characters written to buffer (except trailing zero)
       Fc = 1,  A = error

 DIF:  AFB.DE../..../afbcdehl

 NOTE: API designed for OZ localization (OZ 4.5)

```
## GN_Lut ($8E09) : lookup table
```

 IN:   BHL = lookup table address (B=0, local)
       CDE = key (null terminated string) to search
       A   = options
               A0 = 1, case sensitive

 OUT:   Fc = 0, key found and associated pointer returned in HL
        Fc = 1, A = RC_Eof, no matching string found

 DIF:  .F..D.HL/..../afbcdehl

 NOTE: implemented in OZ5.0

       Lookup table format :
       --------------------- 

       keys have to be alphabetically sorted and ASCII sorted in table if case sensitive option set

               defb    N                               ; number of entries
               defm    "key1",0, pointer1              ; first entry, null terminated followed by a 16bit pointer
               defm    "key2",0, pointer2              ; second entry
               ...
               defm    "keyN",0, pointerN              ; Nth and last entry


```
## GN_Lwr ($9409) : to lower
```

 IN:   A = char

 OUT:  Fc = 0, always, A = lowercase

 DIF:  AF....../..../afbcdehl

```
## GN_M16 ($7209) : 16-bit unsigned multiplication
```

 IN:   HL = multiplicant
       DE = multiplier

 OUT   HL = product

 DIF:  .F....HL/..../afbcdehl

 TODO: could return Fc = 1 if overflow

```
## GN_M24 ($7609) : 24bit unsigned multiplication
```

 IN:   BHL = multiplicant
       CDE = multiplier

 OUT:  BHL = product

 DIF:  .FB...HL/..../afbcdehl

 TODO: could return Fc = 1 if overflow

```
## GN_Mdt ($9009) : get the current machine date and time stamp in internal format
```

 IN:   DE = destination (in MS_S0, MS_S1 or MS_S2)

 OUT:  Fc = 0, always

 DIF:  .F....../..../afbcdehl

 NOTE: implemented in OZ5.0

       About machine internal date time format
       ---------------------------------------

       3 bytes for the time (LSB first), in centiseconds (elapsed since the beginning of the day)
       3 bytes for the date (LSB first), in days (elapsed since an internal weird reference)

       This API provides date and time stamp for filesystem DOR, Creation and Update Keys
       and process date and time stamp

```
## GN_Mov ($8A09) : Move file
```

 IN:   A   = reason, 
               MV_TODIR  (0)   move file to destination path keeping filename
               MV_TOFILE (1)   move file to destination path with a new filename
       BHL = pointer to null-terminated source filename (B=0, local)
       CDE = pointer to null-terminated destination path or new filename (C=0, local)

 OUT:  Fc = 0, success, file moved to new path

       Fc = 1, failure, A = error
              RC_IVF, Invalid filename
              RC_ONF, Invalid directory path name
              RC_EXIS, File already exists

 DIF:  .F....../IX../afbcdehl

```
## GN_Msc ($2009) : miscellaneous time operations
```

 IN:   A=0, MT_CVT : convert source to time to elapse
       A=1, MT_UBT : update base time (used over reset)
       A=2, MT_UPT : get system uptime
       A=3, MT_OWT : update OZ window time

 OUT:  depends on reason call

```
### MT_CVT ($00) : convert source time to elapsed time
```

 IN:   BHL = source time days
       CDE = source time centiseconds/ticks

 OUT:  Fc = 0, success
               BHL = minutes to elapse
               C = seconds to elapse
               A = centiseconds to elapse
       Fc = 1, failure, A = RC_Fail

 DIF:  AFBC..HL/..../afbcdehl

```
### MT_OWT ($03) : update OZ window time
```

 IN:   -

 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### MT_UBT ($01) : update base time over a soft reset
```

 IN:   BHL = additional offset in minutes
       C   = additional offset in seconds

 OUT:  Fc = 0, always

 DIF:  .F....../..../afbcdehl

```
### MT_UPT ($02) : convert uptime
```

 IN:   -

 OUT:  Fc = 0, always
               DE = days since latest hard reset
               H = hours, L = minutes, C = seconds

 DIF:  .F.CDEHL/..../afbcdehl

```
## GN_Nln ($2E09) : send newline to stdout
```

 IN:   -

 OUT:  Fc = 0, always

 DIF:  .F....../....

 NOTE:  call deprecated in OZ5.0, use OS_Nln

```
## GN_Opf ($6009) : open file/resource
```

 IN:   BHL = name
       DE  = buffer for explicit filename
       C   = buffer size
       A   = mode
               OP_IN , open for input
               OP_OUT, open for output
               OP_UP , open for update
               OP_MEM, open memory
               OP_DIR, create directory
               OP_DOR, return DOR information

 OUT:  Fc = 0, success,
               IX = handle
               DE = end of name
               B  = number of segments in name
               C  = number of chars in name
       Fc = 1, failure, A=error

 DIF:  AFBCDE../IX../afbcdehl

```
## GN_Opw ($5209) : open wildcard handler
```

 IN:   BHL = wildcard string
       A   = flags
               A0: backward scan
               A1: return full path

 OUT:  Fc = 0, success, IX = wildcard handle
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
## GN_Pdn ($1209) : write integer as ASCII
```

 IN:   HL = source pointer to 32bit integer if HL>255, else BC = 16bit integer
       DE = destination
               DE = 0, write to stream IX
               DE = 1, write to filter IX
               DE > 255, write at (DE)
       IX = destination handle (if DE<2)
       A  = format
               A0 = 1, disable leading zero blanking
               A1 = 1, output leading space
               A2 = 1, output trailing space
               A4-A7,  numeric field width, 0 as large as required

 OUT:  Fc = 0, success
               DE = destination index/pointer
       Fc = 1, failure, A = error

 DIF:  AF..DE../..../afbcdehl

```
## GN_Pdt ($0809) : convert internal date to ASCII string
```

 IN:   HL = source
       DE = destination
       IX = dest handle (if DE<2)
       A  = format
       B  = format
       C  = delimeter (if A5=1)

 OUT:  Fc = 0, success, DE = output index/ptr
       Fc = 1, failure, A = error

 DIF:  AF..DE../..../afbcdehl

```
## GN_Pfs ($5A09) : parse filename segment
```

 IN:   BHL = filename segment

 OUT:  Fc = 0, success
               BHL = terminating character
               A   = flags
                       A0: extension used
                       A1: filename used
                       A2: explicit directory used
                       A3: current directory (".") used
                       A4: parent directory ("..") used
                       A5: wildcard directory ("//") used
                       A6: device specified
                       A7: wildcards used
       Fc = 1, failure, A = error

 DIF:  AF....HL/..../afbcdehl

```
## GN_Phn ($8609) : put hexadecimal number
```

       convert 32 bits integer to hexadecimal ascii string

 IN:
       HL = 2, convert DEBC to stream IX
       HL > 255, pointer to 32 bits integer to convert

       DE = 0, write string to stream IX
       DE = 1, write string to filter IX
       DE > 255, string is written at (DE)

       A = formatting specifier
               A0 = 1, disable leading zero
               A1 = 1, output leading '$'
               A2 = 1, output trailing 'h'
               A3 = 1, lower case (a-f)
               A6-A7,  range
                       00, output 32 bits (DEBC if HL = 2)
                       11, output 24 bits ( EBC if HL = 2)
                       10, output 16 bits (  BC if HL = 2)
                       01, output 8 bits  (   C if HL = 2)

       IX = optional output handle (if DE = 0,1)

 OUT:  Fc = 0, sucess, DE points to character after result (if DE(in) > 255)
       Fc = 1, failure, A = error
               A = RC_Fail (bad source address)
               A = RC_Hnd (bad handle for stream IX or filter IX)

 DIF:  .F..DE../..../afbcdehl

 NOTE: implemented in OZ5.0

```
## GN_Pmd ($1C09) : set current machine date
```

 IN:   HL  = source
       ABC = date (if HL = 2)

 OUT:  -

 DIF:  .F....../..../afbcdehl

```
## GN_Pmt ($1E09) : set current machine time
```

 IN:   HL  = source
       ABC = time (if HL = 2)
       E   = low byte of assumed date (optional)

 OUT:  Fz=1 if time is consistent with date

 DIF:  .F....../..../afbcdehl

```
## GN_Prs ($5809) : parse filename
```

 IN:   BHL = filename

 OUT:  Fc = 0, success
               B = number of segments
               C = length (including terminator)
               A = flags
                       A0: extension used
                       A1: filename used
                       A2: explicit directory used
                       A3: current directory (".") used
                       A4: parent directory ("..") used
                       A5: wildcard directory ("//") used
                       A6: device specified
                       A7: wildcards used
       Fc = 1, failure, A = error

 DIF:  AFBC..../..../afbcdehl

```
## GN_Ptm ($0C09) : convert internal time to ASCII string
```

 IN:   HL = source
       DE = destination
       IX = destination handle (if DE<2)
       A  = format

 OUT:  DE = output pointer (if DE>255)

```
## GN_Rbe ($3E09) : read byte at extended address
```

 IN:   BHL = address (B = 0, local address)

 OUT:  A = byte

 DIF:  AF....../..../afbcdehl

 NOTE: deprecated in OZ5.0, use OS_Rbe

```
## GN_Ren ($6609) : rename file/directory
```

 IN:   BHL = old name
       DE  = new name

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## GN_Sdo ($0E09) : send date and time to standard output
```

 IN:   HL = time[3] and date[3] in internal machine format

 OUT:  -

 DIF:  AF....../..../afbcdehl

 NOTE: rewritten to work for any memory segment
       optimized by generating string once

```
## GN_Sip ($OE09) : system input line
```

 IN:   DE = buffer
       A = flags
          A0 = 1 : buffer contains data
          A1 = 1 : force insert/overwrite
          A2     : 0=insert, 1=overwrite (if A1=1)
          A3 = 1 : return special characters
          A4 = 1 : return on wrap
          A5 = 1 : single line lock
          A6 = 1 : reverse video
          A7 = 1 : return on insert/overwrite (if A3=1)
       B = buffer length
       C = cursor position (if A0=1)
       L = line width (if A5=1)

 OUT:  A = terminating input char, B = length of line (including null)
       C = cursor position on exit

 DIF:  AFBC..../..../afbcdehl

 NOTE: GN_Sip handles left, right, delete and following combined control key

       S LEFT  goto previous word
       S RIGHT goto next word
       <>DEL   delete full line
       <>LEFT  goto start of line
       <>RIGHT goto EOL
       <>D     delete until EOL
       <>G     delete character under cursor
       <>S     swap case (if alpha)
       <>T     delete word
       <>U     insert character under cursor
       <>V     toggle insert/overwrite

 TODO: lots of unnecessary rendering to set cursor etc. in sll mode

```
## GN_Skc ($3209) : skip character
```

 IN:   A  = char to bypass
       HL = source
       IX = source handle (if HL<2)

 OUT:  Fc = 0, success, HL = end ptr (if HL>255)
       Fc = 1, failure, A  = error

 DIF:  AF....HL/....

```
## GN_Skd ($3409) : skip delimiters in byte sequence
```

 IN:   A  = terminator
       HL = source
       IX = source handle (if HL<2)

 OUT:  Fc = 0, success
               HL = terminator ptr (if HL>255)
               Fz = 1 if terminator seen
       Fc = 1, failure, A = error

 DIF:  AF....HL/....

```
## GN_Skt ($3609) : skip to value
```

 IN:   A  = char
       BC = max search length (0=unlimited)
       HL = source
       IX = source handle (if HL<2)

 OUT:  Fc = 0, success
               BC = remaining length
               HL = search char ptr (if HL>255)
       Fc = 1, failure, A = error

 DIF:  AFBC..HL/....

```
## GN_Soe ($3C09) : write string at extended address to standard output
```

 IN:    BHL = pointer to null-terminated string (B=0 isn't local, it's bank 0)

 OUT:   HL = pointer to null

 DIF:   .F....HL/....

 NOTE:  call deprecated in OZ5.0, use OS_Bout

```
## GN_Sop ($3A09) : write local string to standard output
```

 IN:    HL = local pointer to null-terminated string

 OUT:   HL = pointer to null

 DIF:   .F....HL/....

 NOTE:  call deprecated in OZ5.0, use OS_Sout

```
## GN_Swc ($9609) : swap case
```

 IN:   A = char

 OUT:  Fc = 0, always, A = swapped case

 DIF:  AF....../..../afbcdehl

```
## GN_Uab ($6E09) : unlink alarm from alarm list
```

 IN:   BHL = alarm

 OUT:  Fc = 0, success, Fz = 1, alarm removed
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## GN_Upr ($9209) : to upper
```

 IN:   A = char

 OUT:  Fc = 0, always, A = uppercase

 DIF:  AF....../..../afbcdehl (API)

```
## GN_Wbe ($4009) : write byte at extended address
```

 IN:   A   = byte
       BDE = address

 OUT:  -

 DIF:  .F....../..../afbcdehl

 NOTE: deprecated in OZ5.0, use OS_Wbe

```
## GN_Wcl ($5409) : close wildcard handle
```

 IN:   IX = wildcard handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
## GN_Wfn ($5609) : fetch next wildcard match
```

 IN:   IX = wildcard handle
       DE = buffer for explicit name
       C  = buffer size

 OUT:  Fc = 0, success
               DE = end of name
               B  = number of segments in name
               C  = number of chars in name
               A  = DOR type
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

```
## GN_Win ($7A09) : standard window create with banner and bottom line
```

 IN:   BHL = pointer to 7 byte window definition block (if B=0, then local pointer)

           Window Defintion Block offsets:
           0:  A = window ID (bits 0-3). The ID is in range 1-8
               bit 7=1, 6=1: Draw left & right bars, shelf brackets, banner text, bottom line
               bit 7=1, 6=0: Draw left & right bars, shelf brackets, banner text
               bit 7=0, 6=1: Draw left & right bars
               bit 7=0, 6=0: Just create window space (no visible borders)
               bit 4=1: For all windows: grey screen, then draw window to get a pop-up effect
           1:  X coordinate (upper left corner) of Window (255 to center window)
           2:  Y coordinate (upper left corner) of Window (255 to center window)
           3:  WIDTH of Window (inclusive banner & bottom line)
           4:  HEIGHT of Window (inclusive banner & bottom line)
           5:  low byte, high byte address of window banner text
               Only specified if bit 7 of window ID is enabled.
               Set pointer to 0, if using a dynamic banner (window with different banner each time)

       DE = (optional) "dynamic" pointer to banner (if banner pointer in definition block is 0)
               DE has to point in the same segment as BHL

       Example (Extended window "2" with 8 pixel banner and bottom line):
           defb @11100000 | 2
           DEFW $0000
           DEFW $0811
           DEFW bannerptr

    OUT: Fc = 0, always

 DIF:  .F....../..../afbcdehl

 NOTE: cursor and vertical scrolling are enabled when window is created

```
## GN_Wsm ($5C09) : match filename segment to wildcard string
```

 IN:   DE = segment
       HL = wildcard

 OUT:  Fz=1 if match, DE = segment end, HL = wildcard end
       Fz=0 if miss, DE/HL unchanged

 DIF:  .F..DEHL/..../afbcdehl

```
## GN_Xdl ($4809) : delete an entry from a linked list
```

 IN:   BHL=entry to delete, CDE=previous entry

 OUT:  Fc = 0, success
               BHL = next entry
               CDE = previous entry (=BHL(in))
               Fz = 1 if BHL=0
       Fc=1, A = error

```
## GN_Xin ($4609) : insert an entry into a linked list
```

 IN:   HL = pointer to a 9-byte parameter block
               (HL+0)..(HL+2) entry to insert
               (HL+3)..(HL+5) previous entry
               (HL+6)..(HL+8) next entry

```
## GN_Xnx ($4409) : index next entry in linked list
```

 IN:   BHL=current entry, CDE=previous entry

 OUT:  Fc = 0, success
               BHL = next entry
               CDE = new previous entry (=BHL(in))
               Fz=1 if BHL=0
       Fc=1, A = error

 DIF:  AFBCDEHL/....

```
# OS calls ($XX, $XX03, $XX06) : operating system
```

       This API implements kernel calls shared out 3 major types :
       - one byte, faster, generaly without OS stack frame, jumping to kernel 0
       - two bytes, using OS stack frame, jumping to kernel 1 ($06) or kernel 2 ($03)

```
## OS_Alm ($81) : low level alarm manipulation
```

 IN:   A = reason call

 OUT:  depends on reason call

 NOTE: wrapped by OSFrame on entry

```
### AH_ADEC ($0C), action count decrement
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_AINC ($0B), action count increment
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_ARES ($0D), action count reset
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_CNC ($08), cancel an alarm
```

 IN:   IX = alarm handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, failure, A = RC_Hnd

 DIF:  AF....../IX../afbcdehl

```
### AH_DG1 ($09), ding-dong type 1
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_DG2 ($0A), ding-dong type 2
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_RES ($03), reset alarm enable state
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_REV ($02), revive alarms
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_SDEC ($05), remove symbol
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_SET ($07), set a new alarm
```

 IN:   B  = alarm state (0 active)
       DE = alarm id
       HL = pointer to internal date time (6 bytes)

 OUT:  Fc = 0, success, IX = alarm handle
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
### AH_SINC ($04), display symbol
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_SRES ($06), reset symbol
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### AH_SUSP ($01), suspend alarms 
```

 IN:   -
 
 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
## OS_Axm ($C206) : allocate explicit memory
```

 IN:   IX = MemHandle
       B  = bank
       H  = page address MSB
       L  = number of pages to be allocated

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....H./..../afbcdehl
 
```
## OS_Axp ($D206) : allocate explicit page
```

 IN:   IX = MemHandle
       B  = bank
       H  = page address MSB

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
## OS_Bde ($DA06) : copy bytes to extended address
```

 IN:   (HL)->(BDE), C bytes, B=0 local, HL in S0/S1

 OUT:  -

 DIF:  .F....../....

```
## OS_Bhl ($DC06) : copy bytes from extended address
```

 IN:   (BHL)->(DE), C bytes, B=0 local, DE in S0/S1

 OUT:  -

 DIF:  .F....../....

```
## OS_Bix ($60) : bind in extended address
```

 IN:   BHL = extended address

       B = 0,  HL in segment 0/1 : no change
               HL in segment 2   : caller MS2 bound in MS1
                                   caller MS3 bound in MS2, HL masked to MS1
               HL in segment 3   : caller MS3 bound in MS1
                                   MS2 unchanged, HL masked to MS1
       B <> 0,                   : B bound in MS1
                                   B+1 in MS2, HL masked to MS1

       IY = OSPUSH frame

 OUT:  DE = segments 1 and 2 to be preserved for OS_Box

 DIF:  .FB.DEH./..../afbcdehl

```
## OS_Blp ($D806) : make a bleep
```

 IN:   A = sound count
       B = space count
       C = mark count

 OUT:  Fc = 0, always

 DIF:  .F....../....

```
## OS_Bout ($90) : write null-terminated string block at (B)HL to standard output
```

 IN:   BHL = extended pointer to string to be written
       B = 0, then local address space pointer in caller bank binding

 OUT:  Fc = 0, always (error handler is never provoked)
       HL points at byte after null-terminator

 DIF:  .F....HL/..../afbcdehl

 NOTE: implemented in OZ4.2
       as a faster replacement of GN_Soe

```
## OS_Box ($63) : restore bindings after OS_Bix
```

 IN:   DE = segments 1 and 2 returned by OS_Bix

 OUT:  -

 DIF:  .F....../..../afbcdehl

```
## OS_Bp ($AE03), break point interface
```

 IN:   A   = reason
               BP_SET (0) set breakpoint
               BP_RMV (1) remove breakpoint

       BHL = pointer to break point

 OUT:  Fc = 0, success and IX = breakpoint handle
       Fc = 1, failure and A = error
               RC_Exis, breakpoint already exists
               RC_Hand, cannot allocate a debug handle
               RC_Wp, cannot debug in ROM
               RC_Wrap, cannot set breakpoint in lowram
               RC_Fail, error during removal
               RC_Eof, cannot find debug handle
               RC_Use, breakpoint is running

 DIF: .F....../IX..

 NOTE: implemented in OZ5.0

```
## OS_Bye ($21) : exit current application
```

 IN:   A = return code, use RC_OK for a standard exit

 OUT:  - 

 DIF:  never returns to caller

 NOTE: sequence is OS_Bye -> DC_Bye -> OS_Ent (last process or Index)

```
## OS_Cl ($E806) : internal close
```

 IN:   IX = handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, failure, A  = error

 DIF:  AF....../IX../afbcdehl

```
## OS_Cli ($84) : return CLI status
```

 IN:   -

 OUT:  Fc = 0, always
       A = CLI use count (0 means no active CLI)
       Fz = 1, no active CLI
       Fz = 0, active CLI is running

 DIF:  AF....../..../afbcdehl

 NOTE: previous reason calls deprecated in OZ5.0
       it now always returns CLI count

```
## OS_Dbg ($B203), debugger interface
```

 IN:   -

 OUT:  -

 DIF:  ......../....

 NOTE: implemented in OZ5.0
       Low level, called by RST 08h. On entry, the debug push frame is set.
       It contains IX, af, bc, de, hl and AF. It is followed by 
       an OS pushframe containing main registers (see rst.asm).
       This interface is initiated by GN_Elf with debug execution flag.
       This feature is used in 'dbg' command in Shell or <>ENTER in Index.

```
## OS_Del ($E606) : Delete file/directory
```

 IN:   IX = DOR handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, A = error

 DIF:  .F....../IX../afbcdehl

```
## OS_Dis ($B003), disassemble and follow
```

 IN :  A = options
           A7 = 1, DI_FLW  : follow PC (alter pointer to next instruction according
                                        jump/call and flag/condition)
           A6 = 1, DI_BYE  : catch OS_Bye (exit with RC_Eof if encountered)
           A5 = 1, DI_LWR  : low ram protection check (exit with RC_Wrap)
           A4 = 1, DI_ABS  : absolute address (B = 0, means bank 0)
       BHL = extended address to be decoded (B = 0, local)
       DE  = buffer for null terminated disassembled intruction string
       IX  = registers frame (required if A7=1, see DD_Dbg for frame content)

 OUT:  Fc = 0, success
           DE  = points to byte after null terminator of mnemonic string
           HL  = points to next instruction
           C   = number of bytes decoded for instruction
           A   = type of instruction
               A0 = 1, DO_OZCALL : RST 20h instruction
               A4 = 1, DO_FALSE  : condition is false (for JR, JP, CALL, RET cc)
               A5 = 1, DO_JMP    : jump (absolute or relative) instruction
               A6 = 1, DO_RET    : ret instruction
               A7 = 1, DO_CALL   : call instruction

       Fc = 1, failure
           A = Rc_Fail, illegal instruction (returns NOP string, BHL and C updated)
           A = Rc_Eof,  OS_Bye encountered (if DI_BYE option enabled)
           A = Rc_Wrap, next instruction is in lowram (if DI_LWR option enabled)

 DIF:  AFBCDEHL/....

 NOTE: implemented in OZ5.0

```
## OS_Dly ($D606) : delay a given period
```

 IN :  BC = delay in centiseconds

 OUT:  Fc = 1, always, BC = remaining time, A = error
               RC_TIME ($02), timeout, expected exit
               RC_ESC ($01), escape was pressed
               RC_SUSP ($69), process suspended
               RC_DRAW ($66), suspended and screen corrupted

 DIF:  AFBC..../..../afbcdehl

 NOTE: call rewritten respecting original API with power management
       when waiting, CPU is stopped, idle state managed by Blink
       if machine is switched off during delay, lockup box is displayed if active

```
## OS_Dor ($87) : DOR interface
```

 IN:   A = reason call
               DR_GET ($01), get handle for device
               DR_DUP ($02), duplicate DOR
               DR_SIB ($03), return brother DOR
               DR_SON ($04), return child DOR
               DR_FRE ($05), free DOR handle
               DR_CRE ($06), create blank DOR and insert
               DR_DEL ($07), delete DOR
               DR_INS ($08), insert DOR (link IX as son of BC)
               DR_RD  ($09), read DOR record
               DR_WR  ($0A), write DOR record
               DR_OP  ($0B), open explicit filename
               DR_USE ($0C), check DOR handle in use
               DR_GXF ($0D), guess explicit filename
       BC, DE, HL, IX = arguments

 OUT:  depends on reason call

```
### DR_CRE ($06), create blank DOR and insert
```

 IN:   IX = parent
       B  = DOR type (DM_ or DN_)

 OUT:  Fc = 0, IX = new handle with pointer to a blank DOR
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

 NOTE: blank DOR is allocated, family pointers are null and
       body is padded with DOR terminator (-1)
       DOR handle inherits parents attributes

```
### DR_DEL ($07), delete DOR
```

       for API refer to OS_Del

 NOTE: it is recommended to use OS_Del instead, faster and shorter

```
### DR_DUP ($02), duplicate DOR
```

 IN:   IX = handle

 OUT:  Fc = 0, BC = handle
       Fc = 1, A = error

 DIF:  AFBC..../..../afbcdehl

```
### DR_FRE ($05), free DOR handle
```

 IN:   IX = handle

 OUT:  Fc = 0, IX = 0
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

```
### DR_GET ($01), get handle for device
```

 IN:   -

 OUT:  Fc = 0, IX = handle, A = device DOR type
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

```
### DR_GXF ($0D), guess explicit filename
```

 IN:   IX = handle

 OUT:  Fc = 0, A = type, HL = explicit filename
       Fc = 1, A = RC_Hand

 DIF:  AF....HL/..../afbcdehl

```
### DR_INS ($08), insert DOR (link IX as son of BC)
```

 IN:   BC = parent DOR handle
       IX = DOR handle to be inserted

 OUT:  Fc = 0, success (BC, IX valid)
       Fc = 1, A = error (BC, IX valid)

 DIF:  AF....../..../afbcdehl

```
### DR_OP ($0B), open explicit filename
```

 IN:   DE = filename buffer (in S0 or S1)
       C  = number of segment to parse (0 for all)

 OUT:  Fc = 0, success and
               IX = handle (device type, always)
               A  = DOR type (device, file, directory, EPROM)
               B  = segment count in explicit filename
               HL = last character parsed in filename (separator or null terminator)
       Fc = 1, failure and
               A  = RC_Fail, buffer not in S0 or S1
               A  = RC_Onf, filename does not exist

 DIF:  ...CDE../..IY/afbcdehl

```
### DR_RD ($09), read DOR record
```

 IN:   IX = file handle
       B  = type
       C  = buffer length
       DE = buffer

 OUT:  Fc = 0, C = record length
       Fc = 1, A = RC_Fail

 DIF:  AF.C..../..../afbcdehl

```
### DR_SIB ($03), return brother DOR
```

 IN:   IX = handle

 OUT:  Fc = 0, IX = brother, A = DOR type
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

```
### DR_SON ($04), return child DOR
```

 IN:   IX = handle

 OUT:  Fc = 0, IX = son, A = DOR type
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

```
### DR_USE ($0C), check DOR handle in use
```

 IN:   IX = handle

 OUT:  Fc = 0, -
       Fc = 1, A = RC_Use

 DIF:  AF....../..../afbcdehl

```
### DR_WR ($0A), write DOR record
```

 IN:   IX = file handle
       B  = type
       C  = length of data
       DE = pointer to data

 OUT:  Fc = 0, C = record length
       Fc = 1, A = RC_Fail

 DIF:  AF.C..../..../afbcdehl

```
## OS_Ent ($FA06) : enter an application
```

 IN:
       B  = 0, start new process and
               IX = application static handle
               C  = dynamic process id (Pid)
       B <> 0, start instantiation of stacked process
               HL = process handle
               C  = dynamic process id (Pid)

 OUT:  Fc = 1, error, A = RC_Room or RC_Hand

```
## OS_Ep ($B403) : eprom chip low level interface
```

 IN:   A = reason call
               EP_GEN ($00) : get chip generation
               EP_CID ($03) : get chip identification data
               EP_SER ($06) : sector erase
               EP_CER ($09) : chip erase
               EP_PRB ($0C) : program byte
               EP_WBL ($0F) : write block

 OUT:  depends on reason call

 NOTE:

       About OZ5.0 implementation
       --------------------------

       full rewrite with :
       API devoted to eprom low level i/o only (hardware)
       API related to filearea moved to OS_Epr (software)

       this call level performs checks on slot 3, Vpp, OZ slot conflict
       fixes segment overlapping bugs, i28F008S5 in all slots
       uniformizes generation Id and card subtype... make it simple
       removed UV eprom code

       About screen during eprom write
       -------------------------------

       the screen is turned off while saving a file to flash file area that is in
       the same slot as the OZ ROM. During saving, no interference should happen
       from Blink, because the Blink reads the font bitmaps each 1/100 second:
          When saving a file is part of OZ ROM chip, the font bitmaps are suddenly
          unavailable which creates violent screen flickering during chip command mode.
          Further, and most importantly, avoid Blink doing read-cycles while
          chip is in command mode.
       by switching off the screen, the Blink doesn't read the font bit maps in
       OZ ROM, and the Flash chip can be in command mode without being disturbed
       by the Blink

```
### EP_CER ($09), flash eprom chip erase
```

 IN:
       C = slot number (0, 1, 2 or 3) of Flash Memory Card
 OUT:
       Fc = 0, success
       Fc = 1, failure

 DIF:  AF....../..../afbcdehl

 NOTE: chip erasure is implemented in 29F and 39F series but
       it is easier, shorter, lighter to perform a sector erasure loop

```
### EP_CID ($03), identify eprom chip data
```

 IN:   C = slot number (0, 1, 2 or 3)

 OUT:  Fc = 0, success 
               A  = chip generation
               B  = size in bank
               C  = number of sectors
               H  = manufacturer id
               L  = chip id

       Fc = 1, failure, A = error

 DIF:  AFBC..HL/..../afbcdehl

```
### EP_GEN ($00), identify eprom chip generation
```

 IN:   C = slot number (0, 1, 2 or 3)

 OUT:  Fc = 0, success, A = chip generation
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### EP_PRB ($0C), flash eprom program byte 
```

 IN:
       C   = byte to blow
       E   = eprom generation id (0 will poll)
       BHL = pointer to memory (B=00h-FFh, HL=0000h-3FFFh)

 OUT:
       Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
### EP_SER ($06), flash eprom sector erase
```

 IN:
       B = sector number to be erased (0-15 for 64K sectors, 0-127 for 4K sectors)
       C = slot number (0-3)
 OUT:
       Fc = 0, success
       Fc = 1, failure

 DIF:  AF....../..../afbcdehl

```
### EP_WBL ($0F), flash eprom block write
```

 IN:   C   = eprom generation id (0 will poll)
       DE  = local pointer to start of block (in user address space)
       BHL = extended address to start of destination
       IX  = size of block (1 to 65536 bytes)

 OUT:  Fc = 0, success, C = updated (if null), BHL = first byte after area written
       Fc = 1, failure, A = error

 DIF:  AFBC..HL/..../afbcdehl

```
## OS_Epr ($F006) : eprom filearea interface
```

 IN:   A = reason code
       BC, DE, HL, IX = arguments

 OUT:  Fc = 0, success
       Fc = 1, A = error

 NOTE: call rewritten in OZ5.0
       reason call are only refering to eprom filearea management
       low level flash eprom i/o is performed by OS_Ep

```
### EP_ActSp ($21), amount of active (visible) file space
```

 IN:   C = slot number containing File Eprom Area

 OUT:  Fc = 0, File Eprom available
         DEBC = Active space (amount of visible files) in bytes
                (DE = high 16bit, BC = low 16bit)

       Fc = 1, failure, A = RC_ONF, File Eprom was not found in slot C.

 DIF:  .FBCDE../..../afbcdehl

```
### EP_Count ($27), count total of active and deleted files in filearea
```

 IN:   C = slot number containing File Eprom Area

 OUT:  Fc = 0, File Eprom available
         HL = total of active (visible) files
         DE = total of (marked as) deleted files
         (HL + DE are total files in the file area)

       Fc = 1, failure, A = RC_Onf, File Eprom was not found at slot C

 DIF:  AF..DEHL/..../afbcdehl

 NOTE: NULL file on Intel Flash card is exclued

```
### EP_Delete ($3C), mark eprom file entry as deleted
```

 IN:   BHL = pointer to File Entry (B=00h-FFh, HL=0000h-3FFFh bank offset)

 OUT:  Fc = 0, success, marked as deleted.
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
### EP_Fetch ($09), fetch file image from filearea to file
```

 IN:   IX  = handle of file stream (opened previously with GN_Opf, A = OP_OUT)
       BHL = pointer to Eprom File Entry

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: rewritten in OZ5.0

```
### EP_Find ($0C), find active file in filearea in slot C
```

 IN:   C  = slot number of File Eprom (Area)
       DE = pointer to null-terminated filename to be searched for
         The filename is excl. device name and must begin with '/'

 OUT:  Fc = 0, File Eprom available
         Fz = 1, File Entry found.
              BHL = pointer to File Entry in card at slot
         Fz = 0, No file were found on the File Eprom.
              BHL = pointer to free byte on File Eprom in slot

       Fc = 1, failure, A = RC_Onf, File Eprom was not found at slot C

 DIF:  .FB...HL/..../afbcdehl

```
### EP_First ($12), return first file entry pointer in filearea
```

 IN:   C = slot number containing File Eprom

 OUT:  Fc = 0, File Eprom available
         Fz = 1, File Entry marked as deleted
         Fz = 0, File Entry is active.
         BHL = pointer to first file entry in slot (B=00h-FFh, HL=0000h-3FFFh).
         (NULL file skipped if found on Intel Flash Card)

       Fc = 1,failure, A = RC_Onf, File Eprom was not found in slot, or File Entry not available

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: if the NULL file is identified as the first file, it is skipped and the next
       file entry pointer is automatically returned.

```
### EP_Format ($3F), flash eprom file area formatting
```

 IN:   C = slot number (0, 1, 2 or 3) of Flash Memory Card

 OUT:  Fc = 0, success
               BHL = pointer to File Header for slot C (B = absolute bank of slot).
                    (or pointer to free space in potential new File Area).
               C = size of File Eprom Area in 16K banks

       Fc = 1, failure
             A = RC_ONF (File Eprom Card / Area not available; possibly no card in slot)
             A = RC_ROOM (No room for File Area; all banks used for applications)
             A = RC_NFE (not a recognized Flash Memory Chip)
             A = RC_BER (error occurred when erasing block/sector)
             A = RC_BWR (couldn't write header to Flash Memory)
             A = RC_VPL (Vpp Low Error)

 DIF:  AFBCDEHL/..../afbcdehl 

 NOTE:
       The format of a standard 'oz' file header is as follows:
       --------------------------------------------------------
       $3FC0       $00's until
       $3FF7       $01
       $3FF8       4 byte random id
       $3FFC       size of card in banks (2=32K, 8=128K, 16=256K, 32=512K, 64=1Mb)
       $3FFD       sub-type = flash eprom generation
       $3FFE       'o'
       $3FFF       'z' (file eprom identifier, lower case 'oz')

```
### EP_FreSp ($24), return free space in filearea
```

 IN:   C = slot number containing File Eprom Area

 OUT:  Fc = 0, DEBC = Free space available

       Fc = 1, A = RC_Onf, file Area was not found in slot C
               A = RC_Room, card is full ROM

 DIF:  AFBCDE../..../afbcdehl

```
### EP_Image ($33), return pointer to start of file image
```

 IN:   BHL = pointer to Eprom File Entry in card at slot

 OUT:  Fc = 0, BHL = pointer to start of file image
       Fc = 1, A = RC_Onf

 DIF:  AFB...HL/..../af......

```
### EP_Mount ($53), mount EPROM (API entry)
```

 IN:   C = slot

 OUT:  Fc=0, success
       Fc=1, error

 DIF:  AFBCDEHL/..../afbcdehl
 API:  AF....../..../afbcdehl

```
### EP_New ($36), return BHL pointer to new file entry 
```

 IN:   C = slot number containing File Eprom Area

 OUT:  Fc = 0, BHL = pointer to first byte of free space (B = absolute bank of slot C)

       Fc = 1, A = error (RC_Onf, RC_Room)

 DIF:  AFB...HL/..../af......

```
### EP_Next ($18), return next file entry pointer in filearea
```

 IN:   BHL = pointer to File Entry

 OUT:  BHL = pointer to next file entry on File Eprom in slot, or first byte of empty space
       (B=00h-FFh embedded slot mask, HL=0000h-3FFFh bank offset)

    Fc = 0, File Eprom available
         Fz = 1, File Entry marked as deleted
         Fz = 0, File Entry is active.
    Fc = 1, A = RC_Onf, File Eprom was not found in slot, or File Entry not available

 DIF:  AFB...HL/..../af......

```
### EP_Node ($59), add node to filesystem tree (API entry)
```

 IN:   BHL = pointer to eprom file entry

 OUT:  Fc = 0, success
       Fc = 1, error

 DIF:  AFBCDEHL/..../afbcdehl
 API:  AF....../..../afbcdehl

```
### EP_Prev ($15), return pointer to previous file entry in filearea
```

 IN:   BHL = pointer to current file entry in slot (B=00h-FFh, HL=0000h-3FFFh).

 OUT:  Fc = 0, File Eprom available
         Fz = 1, File Entry marked as deleted
         Fz = 0, File Entry is active.
         BHL = pointer to previous file entry in slot (B=00h-FFh, HL=0000h-3FFFh).

       Fc = 1, failure, A = RC_Onf
         File Eprom was not found in slot, or current File entry was the first
         File Entry (an attempt was made to go beyond the bottom of the file area)

 DIF:  .FB...HL/..../afbcdehl

```
### EP_Req ($06), check for "oz" File Eprom
```

 IN:   C = slot number (0, 1, 2 or 3)

 OUT:  Fc = 0, success (and Fz = 1)
               BHL = pointer to File Header for slot C (B = absolute bank of slot).
                    (or pointer to free space in potential new File Area).
               C = size of File Eprom Area in 16K banks
               A = card subtype (flash generation)
       Fc = 1, failure, A = RC_Onf, potential file area
                        A = RC_Room, no potential file area (B valid)

 DIF:  AFBC..HL/..../........

```
### EP_SaveAs ($4E), eprom file area save as
```

 IN:
           C = slot number (0, 1, 2 or 3)
          IX = RAM file handle
           D = size of file entry name
         BHL = pointer to explicit file entry name string

 OUT:
         Fc = 0, File successfully saved to Flash File Eprom
              BHL = pointer to created File Entry in slot C

         Fc = 1, A = error

 DIF:  AFB...HL/..../afbcdehl (API)
 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: 2022 implementation at sector level, faster, lighter

```
### EP_Size ($2D), return file size of File Entry at pointer BHL
```

 IN:   BHL = Pointer to File Entry in card at slot

 OUT:  Fc = 0, File Eprom available
         Fz = 1, File Entry marked as deleted
         Fz = 0, File Entry active
         CDE = size of file (24bit integer, C = high byte)

       Fc = 1, A = RC_ONF, Eprom was not found in slot, or File Entry not available

 DIF:  AF.CDE../..../af......

```
### EP_Umnt ($56), unmount EPROM file area (API entry)
```

 IN:   C = slot

 OUT:  Fc=0, success
       Fc=1,   A=RC_Use, cannot unmount, file(s) are in use
               A=RC_Onf, no file area in slot

 DIF:  AFBCDEHL/..../afbcdehl
 API:  AF....../..../afbcdehl

```
## OS_Erc ($72) : get error context
```

 IN:   -

 OUT:  Fc = 0, always
       A = last error code
       B = resumption cycle
       C = dynamic id (Pid)

 DIF:  AFBC..../..../afbcdehl
       
```
## OS_Erh ($75) : set error handler
```

 IN:   A  = 0, other values reserved for system use
       HL = error handler entry, HL= 0 use default system error handle in low ram

 OUT:  Fc = 0, always
       A  = old call level
       HL = address of old call level

 DIF:  AF....HL/..../afbcdehl

```
## OS_Esc ($6F) :  examine special condition
```

 IN:   A = reason call

 OUT:  depends on reason call

 DIF:  AF....../..../afbcdehl

```
### SC_ACK ($01), acknowledge escape
```

 IN:   -
 
 OUT:  Fz = 1, there was no escape to acknowledge
       Fz = 0, escape acknowledged

 NOTE: Notice that RC_Esc = SC_Ack, so escape can be acknowledged without reloading A
       This call also reset machine timeout

```
### SC_BIT ($00),test for escape
```

 IN:   -
 
 OUT:  Fc = 1, escape has been pressed, A = RC_Esc
       Fc = 0, escape not pressed

```
### SC_DIS ($06), disable escape detection
```

 IN:   -
 
 OUT:  -

```
### SC_ENA ($05), enable escape detection
```

 IN:   -
 
 OUT:  -

```
### SC_RES ($03), reset escape
```

 IN:   -
 
 OUT:  -

 NOTE: This call resets escape flag without flushing the input buffer

```
### SC_SET ($02), set escape
```

 IN:   -
 
 OUT:  -

 NOTE: This call fires an escape condition

```
### SC_TST ($04), test if escape detection is enabled
```

 IN:   -
 
 OUT:  Fc = 0, always
               A = SC_ENA, if escape is enabled
               A = SC_DIS, if escape is disabled

```
## OS_Exit ($F606) : quit process (application)
```

 IN:
       B  = 0, exit popdown/ugly application and
               IX = application static handle
       B <> 0, start instantiation and force OS_Bye via RC_Quit and
               HL = process handle
               C  = dynamic process id (Pid)

 DIF:  AF....../....


```
## OS_Fat ($B603) : SD card and FAT32 filesystem interface
```

 IN:   A = reason call
               FS_Mount ($00)
               FS_Umnt  ($03)
               FS_Fetch ($06)
               FS_Save  ($09)
               FS_Dir   ($0C)
               FS_Del   ($0F)
               FS_Ren   ($12)
               FS_Free  ($15)
               FS_Size  ($18)
               FS_Fmt   ($1B)
               FS_Req   ($1E)
               FS_Read  ($21)
               FS_Write ($24)
               FS_Next  ($27)
               SD_Init  ($2A) FIXME
               SD_SecRd ($2D)
               SD_SecWr ($30)

 OUT:  depends on reason call

 NOTE: implemented in OZ5.0

 FIXME: OS push frame not implemented on exit...

```
### FS_Del ($0F), delete file
```

 IN:   DEBC = first file cluster

 OUT:  Fc = 0, file successfuly deleted
       Fc = 1, error

 NOTE: file is identified by its first cluster number, this number is unique
       first, clusters are released, marked as free
       then, file entry is marked as deleted in FAT

```
### FS_Fetch ($06), fetch file from FAT32 to filesystem
```

 IN:   IX = file handle

 OUT:  Fc = 0, file successfuly fetched from FAT32
       Fc = 1, error

```
### FS_Free ($15) : return FAT32 free space
```

 IN:   -

 OUT:  Fc = 0, success
               DEBC = free space in K

       Fc = 1, error
               A = RC_Onf, card not mounted
               A = RC_Esc, escape key pressed during computation (if enabled)
               A = RC_Susp, suspended during computation
               A = RC_Time, timeout when reading card (should not occur)

 NOTE: for a 2GB SD card, FAT contains 524288 clusters numbers ($80000) of 4K
       one FAT has 4096 sectors (2MB)
       it may be long to scan a whole large FAT (eg. 2'30" for a 8GB SD card)
       enable escape detection (OS_Esc/SC_ENA) allows escape by key press

```
### FS_Mount ($00), mount FAT32 on SD card
```

 IN:   -

 OUT:  Fc=0, success, SD card mounted on :SDC.0 device
       Fc=1, error
               A = RC_Exis, already mounted
               A = RC_Onf, card not detected
               A = RC_Fail, cannot initialize SD card
               A = RC_Sntx, cannot initialize SD card
               A = RC_Rp, cannot read sector
               A = RC_Ftm, bad format (not FAT32)

 DIF:  AFBCDEHL/IX..

```
### FS_Next ($27), increment sector number, cluster if required
```

 IN:   DEBC = cluster number
       A    = sector number

 OUT:  Fc = 0, success, sector incremented
               DEBC = cluster number
               A    = sector number

       Fc = 1, error
               A = RC_Eof, End Of Cluster
               A = RC_Rp, cannot read sector

```
### FS_Read ($21), read FAT sector API entry
```

 IN:   DEBC = cluster number
       A    = sector number

 OUT:  Fc = 0, success, sector read in SdcSector buffer
       Fc = 1, error

 NOTE: assuming FatInit done with ubCardType and ubFatSecShifts, ubFatSecPerClus, ulFatRootDir set
       read sector A at DEBC

```
### FS_Req ($1E), validate SD card with FAT32
```

 IN:   -

 OUT:  Fc = 0, success, SD card is valid and filesystem is FAT32
       Fc = 1, error
               A = RC_Na, no SPI interface
               A = RC_Onf, card not detected
               A = RC_Fail, cannot initialize SD card
               A = RC_Sntx, cannot initialize SD card
               A = RC_Ftm, bad format (not FAT32)
               A = RC_Rp, cannot read sector

```
### FS_Save ($09), save file
```

 IN:   IX = file handle

 OUT:  Fc = 0, success, file saved
       Fc = 1, error

 NOTE: first cluster number is used as a unique 32bit file identifier (faster than filename compare)

```
### FS_Size ($18), get FAT32 size
```

 IN:   -

 OUT:  Fc = 0, success
               DEBC = size of partition in K
               HL   = cluster size in K

       Fc = 1, error
               A = RC_Onf, card not mounted

```
### FS_Umnt ($03), unmount SD card
```

 IN:   -

 OUT:  Fc=0, success
       Fc=1,   A=RC_Use, cannot unmount, file(s) are in use
               A=RC_Onf, no file area in slot

 DIF:  AFBCDEHL/IX..

```
### FS_Write ($24), write FAT sector API entry
```

 IN:   DEBC = cluster number
       A    = sector number

 OUT:  Fc = 0, success, sector read in SdcSector buffer
       Fc = 1, error

 NOTE: assuming FatInit done with ubCardType and ubFatSecShifts, ubFatSecPerClus, ulFatRootDir set
       read sector A at DEBC

```
### SD_Init ($2A), initialize SD card in SPI mode
```

 IN:   -

 OUT:  Fc = 0, success
               A = card type (0:SDSC, 1:SDHC)
               DEBC = card id

       Fc = 1, failure A = error
                       RC_Na,   hardware error
                       RC_Onf,  CMD0 failed
                       RC_Fail, other CMD failed
                       RC_Time, timeout
                       RC_Esc,  escape
                       RC_Susp, pre-empted

```
### SD_SecRd ($2D), read sector
```

 IN:   A       = card type (0=SDSC, 1=SDHC)
       DEBC    = sector number (CDE for SDSC)
       HL      = buffer (512 bytes required)

 OUT:  Fc = 0, success (A=0)
       Fc = 1, error

 DIF:  AFBCDEHL/..../........

```
### SD_SecWr ($30), write sector
```

 IN:   A       = card type (0=SDSC, 1=SDHC)
       DEBC    = sector number (CDE for SDSC)
       HL      = buffer (512 bytes required)

 OUT:  Fc = 0, success
       Fc = 1, error

 DIF:  AFBCDEHL/..../........

```
## OS_Fc ($8A) : select fast code (fast bank switching)
```

 IN:   A  = 1, select fast bank switching (no other reason implemented)
       C  = segment for bank switching
       DE = address to copy code into
       HL = 0, code to terminate with a RET 
       HL <>0, code to terminate with a JP (HL)

 OUT:  Fc = 0, success, code installed
       Fc = 1, error, A = RC_Unk, bad reason call

 DIF:  AF....../..../afbcdehl

 NOTE: call used by Pipedream

```
## OS_Fdp ($BA06) : file duplicate (sector-level)
```

 IN:   DE = OP_IN  file handle
       IX = OP_OUT file handle

 OUT:  Fc = 0, success, RAM File copied to destination
       Fc = 1, error
         A = RC_Hand, DE or IX is not a file handle
         A = RC_Wp, destination file is write protected
         A = RC_xxx, No Room, I/O error during copying process.

 DIF:  AFBC..HL/..../afbcdehl

 NOTE: call implemented in OZ 5.0

```
## OS_Fma ($C406) : find Memory for Allocation
```

 IN:
       A7 = even bank needed (for segment 0 allocation)
       A6 = fixed memory requested (no subject to swapping)
       A5 = from any slot
       A4 = no offset (calculated by call)
       (A2 used internaly by routine)
       A0-A1 = slot number if A5=0

       H = page offset ($00-$3F)
       L = number of pages wanted (1 to 64)

 OUT:  Fc = 0 if empty memory found at bank B
       Fc = 1, A = RC_Room if not enough space in slot(s)

 DIF:  AFBC..HL/..../afbcdehl

```
## OS_Fn ($7B) : handle functions
```

 IN:   A = reason call
       BC, IX = arguments

 OUT:  Fc = 0, success, IX according reason call
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

 NOTE: 
       Call rewritten for kernel 0 without OS push frame usage.
       With the same API, it provides a better reactivity with less overhead.
       Reason calls are detailed below.

```
### FN_AH ($01), allocate handle
```

 IN:   B = type

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

```
### FN_CH ($05), change handle type
```

 IN:   IX = handle, B = old type, C = new type

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
### FN_FH ($03), free handle
```

 IN:   IX = handle, B = type

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../IX../afbcdehl

```
### FN_GH ($04), get (or find) handle
```

 IN:   IX = start handle, 0 to start from first handle in list
       B = type, 0 for any type
       C = dyn id, 0 for any id

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, A = error (RC_Eof)

 DIF:  AF....../IX../afbcdehl

```
### FN_NH ($06), count free handles
```

 IN:   -

 OUT:  DE = count

 DIF:  AF..DEHL/..../........

 NOTE: used by OS_Frm with IX = -1

```
### FN_VH ($02), verify handle
```

 IN:   IX = handle, B = type

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....../..../afbcdehl

```
## OS_Frm ($48) : file read miscellaneous
```

 IN:   IX = handle (if IX = -1 system values are returned)
       A  = reason code
       DE = destination buffer (0 = return in DEBC)

 OUT:  Fc = 0, success
               DEBC = value (if DE(in) = 0)
               (DE) = value (if DE(in) <>0)
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

```
### FA_BST ($04), buffer status
```

 IN:   IX = COM handle

 OUT:  Fc = 0, success
               HL = RX buffer state (H used, L free)
               DE = TX buffer state (D used, E free)
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

```
### FA_EOF ($03), end of file status or expanded status
```

 IN:   IX = file handle (if IX = -1 system status is returned)

 OUT:  Fz = 1, if end of file reached (or expanded machine)
       Fz = 0, if not EOF (or unexpanded machine)

 DIF:  AF....../..../afbcdehl

```
### FA_EXT ($02), return file size or free RAM
```

 IN:   IX = handle (if IX = -1 system values are returned)
       A  = reason code
       DE = destination buffer (0 = return in DEBC)

 OUT:  Fc = 0, success
               DEBC = value (if DE(in) = 0)
               (DE) = value (if DE(in) <>0)
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

 NOTE:
       if IX <> -1, file size returned is a 32bit integer
       if IX  = -1, free RAM in bytes is returned (32bit integer)

```
### FA_PTR ($01), return sequential pointer or system resources
```

 IN:   IX = handle (if IX = -1 system values are returned)
       A  = reason code
       DE = destination buffer (0 = return in DEBC)

 OUT:  Fc = 0, success
               DEBC = value (if DE(in) = 0)
               (DE) = value (if DE(in) <>0)
       Fc = 1, failure, A = error

 DIF:  AFBCDE../..../afbcdehl

 NOTE:
       if IX <> -1, sequential file pointer returned (32bit integer)
       if IX  = -1, free resources are returned,
       DE(in) = 0,
               DE = number of free handles
               B  = OZ version (major)
               C  = OZ version (minor)
       DE(in) <>0,
               (DE+0) = OZ version (minor)
               (DE+1) = OZ version (major)
               (DE+2) = free handles (word)
       
```
## OS_Fwm ($4B) : file write miscellaneous
```

 IN:   IX = file handle
       A  = reason call
       HL = pointer to a 32bit value

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### FA_Eof ($03), set pointer to end of file
```

 IN:   IX = file handle

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

 NOTE: implemented in OZ5.0

```
### FA_Ext ($02), set file size
```

 IN:   IX = file handle
       HL = pointer to a 32bit value

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### FA_Ptr ($01), write sequential pointer
```

 IN:   IX = file handle
       HL = pointer to a 32bit value

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### FA_Rwd ($04), rewind file
```

 IN:   IX = file handle

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

 NOTE: implemented in OZ5.0

```
## OS_Fxm ($C006) : free explicit memory
```

 IN:   IX = MemHandle
       B  = bank
       H  = page address MSB
       L  = number of contiguous pages to be released

 OUT:  Fc = 0, success
       Fc = 1, A = error

 DIF:  AF....H./..../afbcdehl

```
## OS_Gb ($39) : get byte from stream
```

 IN:   IX = handle

 OUT:  Fc=0, success, A = byte
       Fc=1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## OS_Gbt ($3F) : get byte from stream with timeout
```

 IN:   IX = handle
       BC = timeout

 OUT:  Fc=0, success, A = byte, BC = remaining time
       Fc=1, failure, A = error

 DIF:  AFBC..../..../afbcdehl

```
## OS_Hout ($96) : write hexadecimal byte to standard output
```

 IN:   A = byte to be written

 OUT:  Fc = 0, always (error handler is never provoked)

 DIF:  .F....../..../afbcdehl

 NOTE: implemented in OZ4.5

```
## OS_In ($2A) : read character from standard input without timeout
```

 IN:   -

 OUT:  Fc = 0, success, A  = character read
       Fc = 1, error

 DIF:  AF....../....

```
## OS_Iso ($B806) : key to ISO conversion
```

 IN:   A = key

 OUT:  Fc=0, always
               Fz = 1, converted, A = ISO
               Fz = 0, not converted, A unchanged

 DIF:  AF....../..../afbcdehl

 NOTE: call implemented in OZ5.0

```
## OS_Isq ($D006) : initialize prefix sequence
```

       clear sequence buffer and set buffer pointer

 IN:   HL = sequence buffer (22 bytes required)

 OUT:  -

 DIF:  .F....HL/..../afbcdehl

```
## OS_Kin ($99) : read character from keyboard with timeout
```

 IN:   BC = timeout, -1 for no timeout

 OUT:  Fc = 0, success A = character corresponding to key pressed, BC = remaining time
       Fc = 1, failure A = error (RC_Susp, RC_Time, RC_Esc)

 DIF:  AFBC..../..../afbcdehl

 NOTE: implemented in OZ5.0
       reads input without processing [] and <> keys

```
## OS_Mal ($54) : allocate memory
```

 IN:   A  = 0, anywhere (default)
               others values (internal)
               16-19  : in slot A (MA_SLT|0-3)
               32-255 : in bank A
       BC = allocation size (1-16384)
       IX = memory handle

 OUT:  Fc = 0, success
               BHL = pointer to memory
               C   = segment
       Fc = 1, failure, A = error

 DIF:  AFBC..HL/..../afbcdehl

```
## OS_Map ($F406) : Pipedream map control
```

 IN:   C = reason call

 OUT:  Fc = 0, success
       Fc = 1, error, A = RC_Unk, unknown reason
 
```
### MP_DEF ($02), define a map of default width
```

 IN:   A = window id ASCII ('1' - '6')

 OUT:  Fc = 0, success
               B = default width in pixels (80-256 modulus 8)
               C = width in LORES characters (6 pixels)
       Fc = 1, error
               A = RC_Hand, bad window id

 DIF:  AFBC..../..../afbcdehl

 NOTE: default Pipedream map is at least 80 pixels (OZ 5.0)
       if PA_Map = 'No', Fc = 0, BC = 0 is returned

```
### MP_DEL ($04), delete current map
```

 IN:   -

 OUT:  Fc = 0, success, map deleted
       Fc = 1, A = RC_Fail, no map defined

 DIF:  AF....../....

 NOTE: not implemented before OZ 5.0

```
### MP_GRA ($03), define a map of specific width
```

 IN:   A  = window id ASCII ('1' - '6')
       HL = requested width of map (1-256)

 OUT:  Fc = 0, success
               B  = adjusted width in pixels (0-255 modulus 8, 255 for 256 pixels width)
               C  = width in LORES characters (6 pixels)
               HL = adjusted width in pixels (0-256)

       Fc = 1, error
               A = RC_Hand, bad window id

 DIF:  AFBC..HL/..../afbcdehl

```
### MP_MEM ($05), define a map of specific width associated to a segment
```

 IN:   A  = window id ASCII ('1' - '6')
       B  = segment for extended address
       HL = requested width of map (1-256)

 OUT:  Fc = 0, success
               C   = segment (0-3)
               BHL = extended address of map base in requested segment
               D   = adjusted width in pixels (0-255 modulus 8, 255 for 256 pixels width)
               E   = width in LORES characters (6 pixels)

       Fc = 1, error
               A = RC_Hand, bad window id

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: implemented in OZ4.2

```
### MP_WR ($01), write a line to the map
```

 IN:   DE = row to be written (0-63)
       HL = pixel bytes

 OUT:  -

 DIF:  AF....../....

```
## OS_Mcl ($51) : close memory (free memory pool)
```

 IN:   IX = MemHandle

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
## OS_Mfr ($57) : free memory
```

 IN:   IX  = memory handle
       BC  = deallocation size
       AHL = memory to free

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## OS_Mgb ($5A) : get current bank binding in segment
```

 IN:   C = memory segment specifier (MS_S0, MS_S1, MS_S2 & MS_S3)

 OUT:  Fc = 0, success, B = bank number currently bound to that segment
       Fc = 1, failure, A = RC_Ms, C was not valid

 DIF:  AFB...../..../afbcdehl

 NOTE: deprecated in OZ5.0, use CALL OZ_MGB

```
## OS_Mop ($4E) : open memory (allocate memory pool)
```

 IN:
       A = memory mask arguments (several arguments OR'ed together)
               Destination segment (allocated memory addressed for segment):
                       MM_S0 ($00), segment 0
                       MM_S1 ($40), segment 1
                       MM_S2 ($80), segment 2
                       MM_S3 ($C0), segment 3
               Source of memory (to be allocated)
                       MM_FIX ($02), fixed workspace (not subject to swapping)
                       MM_SLT ($04), explicit slot usage
                       MM_EXC ($10), exclusive use of bank
                       MM_MUL ($20), use multiple banks

       C = bank or slot number if MM_SLT = 1 and MM_MUL = 0
               MC_AS ($00), find an exclusive bank in any slot
               MC_CI ($10), internal memory
               MC_C1 ($01), card 1
               MC_C2 ($02), card 2
               MC_C3 ($03), card 3
               or ($20-$FF), explicit bank

 OUT:  Fc = 0, success, IX = memory handle
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
## OS_MPB ($5D) : set new bank binding in segment
```

 IN:   C = memory segment specifier (MS_S0, MS_S1, MS_S2 & MS_S3)
       B = bank number to bind into this segment ($00 - $FF)

 OUT:  Fc = 0, success, B = bank number previously bound to that segment
       Fc = 1, failure, A = RC_Ms, C was not valid

 DIF:  AFB...../..../afbcdehl

 NOTE: deprecated in OZ5.0, use RST OZ_MPB

```
## OS_Mv ($45) : move bytes between stream and memory
```

 IN:   BC = number of bytes to move (0 none)
       DE = 0, move data from memory starting at HL to the file
       HL = 0, move data from the file to memory starting at DE
       IX = handle of file (or device)

 OUT:  Fc = 0, success (maybe partially)
               BC = number of bytes not moved
               DE = 0, HL = points to next byte to read
               HL = 0, DE = points to next byte to write

       Fc = 1, failure, A = error

 DIF:  AFBCDEHL/..../afbcdehl

```
## OS_Nln ($9F) : Write CRLF to standard output
```

 IN:   -

 OUT:  Fc = 0, always (error handler is never provoked)

 DIF:  .F....../..../afbcdehl

 NOTE: implemented in OZ5.0
       as a faster replacement of GN_Nln

```
## OS_Nq ($45) : enquire
```

 IN:   BC = reason call
               Panel    ($80xx)
               Window   ($83xx)
               Process  ($86xx)
               Memory   ($89xx)
               Director ($8Cxx)
 
 OUT:  depends on reason call

```
### NQ_Ain ($8600), application information
```

 IN:   IX = application static handle

 OUT:  Fc = 0, always, application data
               A = flags
               C = key
               BHL = name
               BDE = DOR

 DIF:  AFBCDEHL/....

```
### NQ_BTL ($8624), read battery status
```

 IN:   -

 OUT:  Fz = 0, battery is good
       Fz = 1, battery is low

 DIF:  .F....../....

 NOTE: implemented in OZ4.5

```
### NQ_COM ($8612), read comms handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### OS_Nq Director ($8Cxx)
```

 NOTE:
       About DC enquiries
       ------------------
       refer to DC_Nq for detailed API

       NQ_Dev ($8C00) fetch current device
       NQ_Dir ($8C03) fetch current directory
       NQ_Fnm ($8C06) fetch current filename match string
       NQ_Dmh ($8C09) fetch Director special memory handle
       NQ_Inp ($8C0C) read std. input handle
       NQ_Out ($8C0F) read std. output handle
       NQ_Prt ($8C12) read printer stream handle
       NQ_Tin ($8C15) read input-T handle
       NQ_Tot ($8C18) read output-T handle
       NQ_Tpr ($8C1B) read printer-T stream handle
       NQ_Chn ($8C1E) read comms handle
       NQ_Prc ($8C21) get process environment (OZ 5.0 and newer)
       NQ_Fmh ($8C24) fetch Filesystem special memory handle (OZ 5.0 and newer)
       NQ_Nam ($8C27) fetch current process name (OZ 5.0 and newer)

```
### NQ_IHN ($8615), read IN handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_KHN ($8603), read keyboard handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_MFS ($8900), memory free space
```

 IN:   IX = memory handle

 OUT:  Fc = 0, success, ABC = free RAM
       Fc = 1, A = RC_Hand, not a memory handle

 DIF:  AFBCDE../....

```
### NQ_MNT ($890C), fetch mount status
```

 IN:   A = slot (0-4)

 OUT:  Fc = 0, device mounted in slot
       Fc = 1, A = RC_Onf, not found, unmounted

 DIF:  .F....../....

 NOTE: implemented in OZ5.0

```
### NQ_NHN ($860C), read null handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_OHN ($8618), read OUT handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### OS_Nq Panel ($80xx)
```

 IN:   A  = buffer length
       BC = parameter identifier ($8001-$8058 and $8080-$80BE)
       DE =  2, return data in DE
       DE <> 2, return data at (DE)

 OUT:  Fc = 0, success
               A = length of data
               data returned in DE or at (DE)
       Fc = 1, failure, A = error

 DIF:  AF..DE../..../afbcdehl

 NOTE:
       About Panel parameters
       ----------------------
       valid for enquiry (OS_Nq) and specify (OS_Sp)
       parameters marked with * are preserved by soft reset 

     * PA_Mct ($8001) Timeout in minutes
     * PA_Rep ($8002) Repeat rate
     * PA_Kcl ($8003) Keyclick 'Y' or 'N'
     * PA_Snd ($8004) Sound 'Y' or 'N'
     * PA_Bad ($8005) Default bad process size in K
     * PA_Loc ($8006) Country keyboard letter
               CL_US 'A' United states
               CL_FR 'F' France
               CL_DE 'G' Germany
               CL_UK 'U' England
               CL_DK 'D' Denmark
               CL_SE 'S' Sweden
               CL_IT 'I' Italy
               CL_SP 'P' Spain
               CL_JP 'J' Japan
               CL_IS 'C' Iceland
               CL_NO 'N' Norway
               CL_CH 'W' Chweiss
               CL_TR 'T' Turkey
               CL_FI 'L' Finland
     * PA_Rwd ($8007) Rounded windows (OZ5.0)
     * PA_Psw ($8008) Lockup password (OZ5.0)
       PA_Iov ($8010) Insert/Overtype 'I' or 'O'
       PA_Dat ($8011) Date format 'E' or 'A'
       PA_Map ($8012) PipeDream map 'Y' or 'N'
       PA_Msz ($8013) Map size in pixels
       PA_Dir ($8014) Default directory
       PA_Dev ($8015) Default device
       PA_Txb ($8016) Transmit baud rate - binary
       PA_Rxb ($8017) Receive baud rate - binary
       PA_Xon ($8018) Xon/Xoff 'Y' or 'N'
       PA_Par ($8019) Parity 'O', 'E', 'M', 'S' or 'N'
       PA_Ptr ($8020) Printer name
       PA_Alf ($8021) Allow Linefeed, 'Y' or 'N'
       PA_Pon ($8022) Printer On sequence
       PA_Pof ($8023) Printer Off sequence
       PA_Eop ($8024) End of page sequence
       PA_Mip ($8025) HMI prefix sequence
       PA_Mis ($8026) HMI suffix sequence
       PA_Mio ($8027) HMI offset sequence

       format of each highlight sequence is as follows :
       PA_On<n>  Highlight ON sequence
       PA_Of<n>  Highlight OFF sequence
       PA_Oc<n>  OFF at CR 'Y' or 'N'
       PA_Oh<n>  Placeholder (internal)

       PA_On1 ($8028) Underline
       PA_Of1 ($8029)
       PA_Oc1 ($802A)
       PA_Oh1 ($802B)
       PA_On2 ($802C) Bold
       PA_Of2 ($802D)
       PA_Oc2 ($802E)
       PA_Oh2 ($802F)
       PA_On3 ($8030) Extended sequence
       PA_Of3 ($8031)
       PA_Oc3 ($8032)
       PA_Oh3 ($8033)
       PA_On4 ($8034) Italics
       PA_Of4 ($8035)
       PA_Oc4 ($8036)
       PA_Oh4 ($8037)
       PA_On5 ($8038) Subscript
       PA_Of5 ($8039)
       PA_Oc5 ($803A)
       PA_Oh5 ($803B)
       PA_On6 ($803C) Superscript
       PA_Of6 ($803D)
       PA_Oc6 ($803E)
       PA_Oh6 ($803F)
       PA_On7 ($8040) Alternate font
       PA_Of7 ($8041)
       PA_Oc7 ($8042)
       PA_Oh7 ($8043)
       PA_On8 ($8044) User Defined
       PA_Of8 ($8045)
       PA_Oc8 ($8046)
       PA_Oh8 ($8047)

       format of each translation code sequence is as follows :
       PA_Tr<n>  Translate from character
       PA_Ts<n>  Translate to sequence

       PA_Tr1 ($8048) Row 1, Entry A
       PA_Ts1 ($8049)
       PA_Tr2 ($804A)              B
       PA_Ts2 ($804B)
       PA_Tr3 ($804C)              C
       PA_Ts3 ($804D)
       PA_Tr4 ($804E) Row 2, Entry A
       PA_Ts4 ($804F)
       PA_Tr5 ($8050)              B
       PA_Ts5 ($8051)
       PA_Tr6 ($8052)              C
       PA_Ts6 ($8053)
       PA_Tr7 ($8054) Row 3, Entry A
       PA_Ts7 ($8055)
       PA_Tr8 ($8056)              B
       PA_Ts8 ($8057)
       PA_Tr9 ($8058)              C
       PA_Ts9 ($8059)

       ISO translations 1 - 32 :
       PA_Ts<n> = PA_Tr<n> + 1

       PA_Tr10 ($8080) Row 1, Entry 1    (ISO 1)
       PA_Tr11 ($8082)              2    (ISO 2)
       PA_Tr12 ($8084)              3    (ISO 3)
       PA_Tr13 ($8086)              4    ...
       PA_Tr14 ($8088)              5
       PA_Tr15 ($808A)              6
       PA_Tr16 ($808C)              7
       PA_Tr17 ($808E)              8
       PA_Tr18 ($8090) Row 2, Entry 1
       PA_Tr19 ($8092)              2
       PA_Tr20 ($8094)              3
       PA_Tr21 ($8096)              4
       PA_Tr22 ($8098)              5
       PA_Tr23 ($809A)              6
       PA_Tr24 ($809C)              7
       PA_Tr25 ($809E)              8
       PA_Tr26 ($80A0) Row 3, Entry 1
       PA_Tr27 ($80A2)              2
       PA_Tr28 ($80A4)              3
       PA_Tr29 ($80A6)              4
       PA_Tr30 ($80A8)              5
       PA_Tr31 ($80AA)              6
       PA_Tr32 ($80AC)              7
       PA_Tr33 ($80AE)              8
       PA_Tr34 ($80B0) Row 4, Entry 1
       PA_Tr35 ($80B2)              2    ...
       PA_Tr36 ($80B4)              3    ...
       PA_Tr37 ($80B6)              4    (ISO 28)
       PA_Tr38 ($80B8)              5    (ISO 29)
       PA_Tr39 ($80BA)              6    (ISO 30)
       PA_Tr40 ($80BC)              7    (ISO 31)
       PA_Tr41 ($80BE)              8    (ISO 32)

```
### NQ_PHN ($8609), read printer indirected handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_RDS ($8306), read text from the screen
```

 IN:   DE = buffer
       HL = number of bytes to read

 OUT:  -

 DIF:  .F....../..../afbcdehl

```
### NQ_RHN ($861B), read direct printer handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_ROZ ($8621), read OZ revision number
```

 IN:   -

 OUT:  DEBC= 32 bits revision number

 DIF:  .FBCDE../....

 VER:  4.5+ 

```
### NQ_SHN ($8606), read screen handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_SLT ($8903), return slot and bank configuration
```

 IN:   D = slot
       E = bank

 OUT:  Fc = 0, always, A = configuration

 DIF:  AF....../....

```
### NQ_THN ($862A), read terminal handle
```

 IN:   BC = reason

 OUT:  Fc = 0, always, IX = handle

 DIF:  AF....../IX../afbcdehl

```
### NQ_TOZ ($8627), read OZ compilation timestamp
```

 IN:   -

 OUT:  CDE = date (internal format)
       BHL = time (internal format)

 DIF:  .FBCDEHL/....

 NOTE: implemented in OZ5.0

```
### NQ_VOZ ($861E), application validates running OZ
```

 IN:   A = OZ version number, eg. $45 for OZV4.5

 OUT:  Fc = 0, Application is running on accepted OZ
       Fc = 1, A = RC_Voz (fatal, application has to suicide)

 DIF:  .F....../....

 VER:  4.5+

```
### NQ_WAI ($860F), who am I ?
```

 IN:   BC = NQ_WAI

 OUT:  IX = application static handle
       C  = PID

 DIF:  .FBC..../IX..

```
### NQ_WBOX ($8300), return window information
```

 IN:   A = window id, 0 for current window

 OUT:  A = window id
       C = width
       B = depth
       E = offset from left of screen (always 0)
       D = offset from top of screen (always 0)

 DIF:  AFBCDE../..../afbcdehl

```
### NQ_WCUR ($8303), return cursor information
```

 IN:   A = window id, 0 for current window

 OUT:  A = window id
       C = cursor X
       B = cursor Y
       D = bit 7 set if cursor is ON
       E = number of remaining rows after cursor (0 = last row)

 DIF:  AFBCDE../..../afbcdehl

 NOTE: new OZ 5.0 implementation
       E = number of remaining rows after cursor
       it is useful when a page wait has to be managed (E = 0)

```
## OS_Off ($EC06) : switch machine off
```

 IN:   -

 OUT:  -

 DIF:  .F....../..../afbcdehl

```
## OS_Op ($EA06) : internal open
```

 IN:
       A = OP_IN  ($01), open file for input
       A = OP_OUT ($02), open (create) file for output, IX=(directory) DOR handle, HL=filename
       A = OP_UP  ($03), open for update, IX=(file) DOR handle
       A = OP_MEM ($04), open memory (for input), BHL=memory, C=size
       A = OP_DIR ($05), create directory, IX=(directory) DOR handle, HL=filename
       A = OP_DOR ($06), create DOR node

```
### OP_DIR ($05), create directory
```

 IN:   IX = parent handle
       HL = name

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### OP_DOR ($06), create node in filesystem
```

 IN:   IX = parent handle
       HL = name
       B  = minor type

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

 NOTE: implemented in OZ5.0

```
### OP_IN ($01), open file for input
```

 IN:   IX = handle

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### OP_MEM ($04), open memory
```

 IN:   BHL = memory
       C   = length

 OUT:  Fc = 0, success, IX = file handle
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

 NOTE: memory area is copied to file (without DOR header).
       file is not visible in the filesystem tree.
       handle is accessed like file data without DOR function.
       name, timestamps operations cannot be performed but
       OS_FWM, OS_FRM can be used.

```
### OP_OUT ($02), open file for output
```

 IN:   IX = handle

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
### OP_UP ($03), open file for update
```

 IN:   IX = handle

 OUT:  Fc = 0, success, IX = handle
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## OS_Out ($27) : write character to standard output
```

 IN:   A = character to be written

 OUT:  Fc = 0, always (error handler is never provoked)

 DIF:  .F....../..../afbcdehl

 NOTE:
       About OS_Out & al.
       ------------------

       OS_Out & al have been completly rewritten without OSFrame usage.
       OS_Out achieves 39% speed increase vs OZ 3.
       Moreover, GN_Sop has been moved to kernel and redesigned as OS_Sout.
       It allows more than twice the original speed (224%).
       It fixes the bug when a string crosses the bank boundary.
       OS_Bout replaces GN_Soe.
       OS_Pout is the fastest and easiest way to output a string.
       OS_Hout output A value as an hexadecimal string.
       
```
## OS_Pb ($3C) : write byte to stream
```

 IN:   IX = handle
       A  = byte

 OUT:  Fc=0, success
       Fc=1, failure, A = error

 DIF:  AF....../..../afbcdehl

```
## OS_Pbt ($42) : write byte to stream with timeout
```

 IN:   IX = handle
       A  = byte to write
       BC = timeout

 OUT:  Fc=0, success, BC = remaining time
       Fc=1, failure, A = error

 DIF:  AFBC..../..../afbcdehl

```
## OS_Ploz ($C606) : poll for OZ usage in slot
```

 IN:   A = slot number (0-3)

 OUT:  Fz = 0, if OZ is running in slot, otherwise Fz = 1

 DIF:  .F....../.... (Fz changed, Fc = 0)

```
## OS_Poll ($FC06) : poll for an application
```

 IN:   IX = current application, 0 for start of list

 OUT:  Fc = 0, success, IX = next application
       Fc = 1, A = error if fail

 DIF:  .F....../IX..

 NOTE: MTH pointers are set from application DOR
       read note about application static handle

```
## OS_Pout ($93) : write embedded null-terminated string to standard output
```

 IN:   -

 OUT:  Fc = 0, always (error handler is never provoked)

 DIF:  .F....../..../afbcdehl

 NOTE: implemented in OZ4.2
       string is embedded at caller (PC) following this system call

```
## OS_Prn ($AC03) : send character directly to printer filter
```

 IN:   A = character to be written to printer filter

 OUT:  Fc = 0, success
       Fc = 1, failure, A = RC_ESC or RC_WP

 DIF:  AFBCDEHL/..../af......
 API:  AF....../..../afbcdehl

 NOTE: call implemented in OZ5.0
       rewrite of OS_PRT without screen driver usage for ENQ buffer
       ENQ Buffer (prefix sequence) is now in low ram

       prefix (ENQ) is not stored in buffer but in PrtSeqPrefix
       structure of buffer (PrtSequence)

       prefix sequence
       ---------------
       [0] length of sequence
       [1] buffer pointer
       [2] char[0]
       ...
       [12] char[10]


```
## OS_Prt ($24) : send character to printer filter
```

 IN:   A = character

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF....../..../afbcdehl

 NOTE: this call may be redirected by the CLI

```
## OS_Pur ($33) : purge keyboard buffer
```

 IN:    -

 OUT:   Fc = 0, always

 DIF:   .F....../....

```
## OS_Rbe ($A2) : read byte at extended address
```

 IN:   BHL = extended address, B = 0, local

 OUT:  A = (BHL)

 DIF:  AF....../....

 NOTE: implemeted in OZ5.0 as a faster GN_Rbe replacement

```
## OS_Ren ($E406) : file rename
```

 IN:   IX = file handle
       HL = pointer to null terminated new filename

 OUT:  Fc = 0, success
       Fc = A, error and handle is released (IX = 0)

 DIF:  .F....../..../afbcdehl

```
## OS_Sci ($D406) : alter screen information
```

 IN:   A = reason code
               SC_LR0 ($01) : LORES0 (512 bytes granularity, 13 bits width)
               SC_LR1 ($02) : LORES1 (4K granularity, 10 bits width)
               SC_HR0 ($03) : HIRES0 (8K granularity, 9 bits  width)
               SC_HR1 ($04) : HIRES1 (2K granularity, 11 bits width)
               SC_SBR ($05) : screen base (2K granularity, 11 bits width)
       B  = 0, get address pointer
       B <> 0, set address pointer at BHL (L = 0, always)

 OUT:  Fc = 0, success, BHL = old address pointer
       Fc = 1, failure, A = error

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: granularity depends on register width
       corresponding to the most significant bits of 22bit physical address

```
## OS_SI ($8D) : low level serial interface
```

 IN :  L = reason code and A, BC, DE, IX according the reason

 OUT:  depends on the reason called

```
### SI_ENQ (L = $0F), serial interface buffer status enquiry
```

 IN :  -

 OUT:  BC = TxStatus (B used, C free)
       DE = RxStatus (D used, E free)
       A = int status
       
 DIF:  AFBCDE../..../afbcdehl

```
### SI_FRX (L = $15), flush RX buffer
```
 
 IN :  -

 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### SI_FTX (L = $12), flush TX buffer
```
 
 IN :  -

 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

 NOTE: this call does nothing
       since there is no more TX buffer in OZ5.0

```
### SI_GBT (L = $09), receive a byte from serial interface
```

 IN:   BC = timeout in centiseconds

 OUT:  Fc = 0, success, A = byte read from serial port
       Fc = 1, A = error (RC_Time, RC_Esc, RC_Susp, RC_Draw)

 DIF:  AFBC..HL/..../afbcdehl

```
### SI_GX (L = $1B), get multiple bytes from serial port using default timeout
```

 IN:   BC = number of byte to get from serial port, maximum 16384, one bank
       DE = destination

 OUT:  Fc = 0, success, all bytes read, BC = remaining timeout
       Fc = 1, A  = error (RC_Na, RC_Time, RC_Esc, RC_Susp, RC_Draw)
               BC = number of bytes not read

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: implemented in OZ4.5

```
### SI_GXT (L = $21), get multiple bytes until ESC+terminator using default timeout
```

 IN:   BC = number of byte to get from serial port, maximum 16384, one bank
       DE = destination

 OUT:  Fc = 0, success
               Fz = 1, BC <> 0, A = ESC terminator encountered, number of bytes not read
               Fz = 0, A  = $FF
               BC = 0, all bytes read, no terminator encountered
               HL = points at last byte fetched + 1

       Fc = 1, A = error (RC_Na, RC_Time, RC_Esc, RC_Susp, RC_Draw)
               BC = number of bytes not read

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: implemented in OZ4.5
       ESC+B is handled directly by call

```
### SI_HRD (L = $00), serial interface hard reset
```

 IN :  -

 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### SI_INT (L = $06), interrupt entry, send XOFF
```

 IN:   -

 OUT:  Fc = 0, success
       Fc = 1, A= error  (RC_Time, RC_Esc, RC_Susp, RC_Draw)

 DIF:  AFBC..HL/..../afbcdehl

```
### SI_PBT (L = $0C), send a byte to serial interface
```

 IN:   A  = byte to be written
       BC = timeout in centiseconds, if -1 use default timeout

 OUT:  Fc = 0, success
       Fc = 1, A = error (RC_Time, RC_Esc, RC_Susp, RC_Draw)

 DIF:  AFBC..HL/..../afbcdehl

```
### SI_PX (L = $1E), put multiple bytes to serial port using default timeout
```

 IN:   BC = number of byte to write to serial port, maximum 16384, one bank
       DE = input

 OUT:  Fc = 0, success, all bytes written, BC = remaining timeout
       Fc = 1, A  = error (RC_Na, RC_Time, RC_Esc, RC_Susp, RC_Draw)
               BC = number of bytes not written

 DIF:  AFBCDEHL/..../afbcdehl

 NOTE: implemented in OZ4.5

```
### SI_SFT (L = $03), serial interface soft reset
```

 IN :  -

 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### SI_STA (L = $27), return serial port connection status
```

 IN :  -

 OUT:  Fc = 0, always
               A = status, DCD and CTS levels returned
               Fz = 0, serial port is connected
               Fz = 1, serial port is not connected

 DIF:  AF....../..../afbcdehl

 NOTE: implemented in OZ5.0
       
```
### SI_TMO (L = $18), set serial port timeout
```

 IN : BC = timeout in centiseconds

 OUT: Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
### SI_XIN (L = $24), examine serial port input
```

 IN :  -

 OUT:  Fc = 0, serial port buffer contains data
       Fc = 1, A = RC_Eof, buffer is empty

 DIF:  AF....../..../afbcdehl

 NOTE: implemented in OZ5.0
       
```
## OS_Sout ($99) : write null-terminated string block at HL to standard output
```

 IN:   HL = pointer to string to be written (not crossing segments)

 OUT:  Fc = 0, always (error handler is never provoked)
       HL points at byte after null-terminator

 DIF:  .F....HL/..../afbcdehl

 NOTE: implemented in OZ5.0
       as a faster replacement of GN_Sop

```
## OS_Sp ($69) : specify
```

 IN:   BC = reason call
               PA_Gfi   ($8000)
               Panel    ($80xx)
               Director ($8Cxx)
 
 OUT:  depends on reason call

```
### PA_GFI ($8000), apply panel settings
```

 IN:   -

 OUT:  Fc = 0, always successful

 DIF:  .F....../....

 NOTE: PA_GFI stands for "Go for it!"

```
### OS_Sp Director ($8Cxx)
```

 NOTE:
       About DC specifies
       ------------------
       refer to DC_Sp for detailed API

       SP_Dev ($8C00) set current device
       SP_Dir ($8C03) set current directory
       SP_Fnm ($8C06) set current filename match string

```
### OS_Sp Panel ($80xx)
```

 IN:   BC = parameter identifier ($8001-$8058 and $8080-$80BE)
       A  = data length (if A = 255, parameter is reset to default value)
       HL = pointer to data

 OUT:  Fc = 0, success
       Fc = 1, failure, A = error

 DIF:  AF..DE../..../afbcdehl

 NOTE:
       About Panel parameters
       ----------------------
       valid for enquiry (OS_Nq) and specify (OS_Sp)
       parameters marked with * are preserved by soft reset 

     * PA_Mct ($8001) Timeout in minutes
     * PA_Rep ($8002) Repeat rate
     * PA_Kcl ($8003) Keyclick 'Y' or 'N'
     * PA_Snd ($8004) Sound 'Y' or 'N'
     * PA_Bad ($8005) Default bad process size in K
     * PA_Loc ($8006) Country keyboard letter
               CL_US 'A' United states
               CL_FR 'F' France
               CL_DE 'G' Germany
               CL_UK 'U' England
               CL_DK 'D' Denmark
               CL_SE 'S' Sweden
               CL_IT 'I' Italy
               CL_SP 'P' Spain
               CL_JP 'J' Japan
               CL_IS 'C' Iceland
               CL_NO 'N' Norway
               CL_CH 'W' Chweiss
               CL_TR 'T' Turkey
               CL_FI 'L' Finland
     * PA_Rwd ($8007) Rounded windows (OZ5.0)
     * PA_Psw ($8008) Lockup password (OZ5.0)
       PA_Iov ($8010) Insert/Overtype 'I' or 'O'
       PA_Dat ($8011) Date format 'E' or 'A'
       PA_Map ($8012) PipeDream map 'Y' or 'N'
       PA_Msz ($8013) Map size in pixels
       PA_Dir ($8014) Default directory
       PA_Dev ($8015) Default device
       PA_Txb ($8016) Transmit baud rate - binary
       PA_Rxb ($8017) Receive baud rate - binary
       PA_Xon ($8018) Xon/Xoff 'Y' or 'N'
       PA_Par ($8019) Parity 'O', 'E', 'M', 'S' or 'N'
       PA_Ptr ($8020) Printer name
       PA_Alf ($8021) Allow Linefeed, 'Y' or 'N'
       PA_Pon ($8022) Printer On sequence
       PA_Pof ($8023) Printer Off sequence
       PA_Eop ($8024) End of page sequence
       PA_Mip ($8025) HMI prefix sequence
       PA_Mis ($8026) HMI suffix sequence
       PA_Mio ($8027) HMI offset sequence

       format of each highlight sequence is as follows :
       PA_On<n>  Highlight ON sequence
       PA_Of<n>  Highlight OFF sequence
       PA_Oc<n>  OFF at CR 'Y' or 'N'
       PA_Oh<n>  Placeholder (internal)

       PA_On1 ($8028) Underline
       PA_Of1 ($8029)
       PA_Oc1 ($802A)
       PA_Oh1 ($802B)
       PA_On2 ($802C) Bold
       PA_Of2 ($802D)
       PA_Oc2 ($802E)
       PA_Oh2 ($802F)
       PA_On3 ($8030) Extended sequence
       PA_Of3 ($8031)
       PA_Oc3 ($8032)
       PA_Oh3 ($8033)
       PA_On4 ($8034) Italics
       PA_Of4 ($8035)
       PA_Oc4 ($8036)
       PA_Oh4 ($8037)
       PA_On5 ($8038) Subscript
       PA_Of5 ($8039)
       PA_Oc5 ($803A)
       PA_Oh5 ($803B)
       PA_On6 ($803C) Superscript
       PA_Of6 ($803D)
       PA_Oc6 ($803E)
       PA_Oh6 ($803F)
       PA_On7 ($8040) Alternate font
       PA_Of7 ($8041)
       PA_Oc7 ($8042)
       PA_Oh7 ($8043)
       PA_On8 ($8044) User Defined
       PA_Of8 ($8045)
       PA_Oc8 ($8046)
       PA_Oh8 ($8047)

       format of each translation code sequence is as follows :
       PA_Tr<n>  Translate from character
       PA_Ts<n>  Translate to sequence

       PA_Tr1 ($8048) Row 1, Entry A
       PA_Ts1 ($8049)
       PA_Tr2 ($804A)              B
       PA_Ts2 ($804B)
       PA_Tr3 ($804C)              C
       PA_Ts3 ($804D)
       PA_Tr4 ($804E) Row 2, Entry A
       PA_Ts4 ($804F)
       PA_Tr5 ($8050)              B
       PA_Ts5 ($8051)
       PA_Tr6 ($8052)              C
       PA_Ts6 ($8053)
       PA_Tr7 ($8054) Row 3, Entry A
       PA_Ts7 ($8055)
       PA_Tr8 ($8056)              B
       PA_Ts8 ($8057)
       PA_Tr9 ($8058)              C
       PA_Ts9 ($8059)

       ISO translations 1 - 32 :
       PA_Ts<n> = PA_Tr<n> + 1

       PA_Tr10 ($8080) Row 1, Entry 1    (ISO 1)
       PA_Tr11 ($8082)              2    (ISO 2)
       PA_Tr12 ($8084)              3    (ISO 3)
       PA_Tr13 ($8086)              4    ...
       PA_Tr14 ($8088)              5
       PA_Tr15 ($808A)              6
       PA_Tr16 ($808C)              7
       PA_Tr17 ($808E)              8
       PA_Tr18 ($8090) Row 2, Entry 1
       PA_Tr19 ($8092)              2
       PA_Tr20 ($8094)              3
       PA_Tr21 ($8096)              4
       PA_Tr22 ($8098)              5
       PA_Tr23 ($809A)              6
       PA_Tr24 ($809C)              7
       PA_Tr25 ($809E)              8
       PA_Tr26 ($80A0) Row 3, Entry 1
       PA_Tr27 ($80A2)              2
       PA_Tr28 ($80A4)              3
       PA_Tr29 ($80A6)              4
       PA_Tr30 ($80A8)              5
       PA_Tr31 ($80AA)              6
       PA_Tr32 ($80AC)              7
       PA_Tr33 ($80AE)              8
       PA_Tr34 ($80B0) Row 4, Entry 1
       PA_Tr35 ($80B2)              2    ...
       PA_Tr36 ($80B4)              3    ...
       PA_Tr37 ($80B6)              4    (ISO 28)
       PA_Tr38 ($80B8)              5    (ISO 29)
       PA_Tr39 ($80BA)              6    (ISO 30)
       PA_Tr40 ($80BC)              7    (ISO 31)
       PA_Tr41 ($80BE)              8    (ISO 32)

```
## OS_Sr ($6C) : save & restore
```

 IN:   A = reason code:

 NOTE: this call implements miscellaneous functions

       SR_SUS ($01) Save user screen
       SR_WPD ($03) Write parameter data (mailbox)
       SR_RPD ($04) Read parameter data (mailbox)
       SR_FUS ($05) Free user screen
       SR_CRM ($06) Remove card (not implemented)
       SR_CIN ($07) Insert card (not implemented)
       SR_PWT ($08) Page wait
       SR_RND ($09) Occasionally a random number
       SR_SRS ($0A) Set random seed

```
### SR_FUS ($05), free user screen
```

 IN:   IX = screen image handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

 NOTE: this reason call release memory used by user screen saved
       but does not restore user screen

```
### SR_PWT ($08), page wait
```

 IN:   A = SR_PWT

 OUT:  like OS_In

 DIF:  AF....../..../afbcdehl

 NOTE:
       this reason call displays the PAGE WAIT message in topic window
       and acts like OS_In
       it is disabled when CLI is active

```
### SR_RND ($09), generate a 32 bit random number
```

 IN:   -

 OUT:  DEBC = random number

 DIF:  AFBCDE../..../afbcdehl

 NOTE:
       rewritten in OZ5.0, a random long integer is given on each iteration
       
       Generates the next 32-bit prng in the sequence, given seed setup during reset.
       This random number generator is a 32-bit XorShift RNG as described by Marsaglia:

       http://www.jstatsoft.org/v08/i14/paper

       The triple used is (L,R,L) = (8,9,23) which is very quickly implemented on the z80.

```
### SR_RPD ($04), read mailbox
```

 IN:   DE  = name
       BHL = buffer
       C   = bufsize

 OUT:  Fc = 0, success, mail copied to buffer
       Fc = 1, error, A = RC_Fail

 DIF:  AF....../..../afbcdehl

```
### SR_RUS ($02), restore user screen
```

 IN:   IX = screen image handle

 OUT:  Fc = 0, success, IX = 0
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
### SR_SRS ($0A), set random seed
```

 IN:   DEBC = random seed

 OUT:  Fc = 0, always

 DIF:  .F....../....

```
### SR_SUS ($01), save user screen
```

 IN:   -

 OUT:  Fc = 0, success, IX = screen image handle
       Fc = 1, failure, A = error

 DIF:  AF....../IX../afbcdehl

```
### SR_WPD ($03), write mailbox
```

 IN:   DE  = name of information type, 0=clear mailbox
       BHL = data
       C   = data length

 OUT:  Fc = 0, always

 DIF:  AF....../..../afbcdehl

```
## OS_Stk ($F806) : stack file current process
```

 IN:   -

 OUT:  Fc = 0, success
               HL = process handle
               B  = 1, process stacked

 DIF:  AFB...HL/..../afbcdehl

```
## OS_Tin ($2D) : read character from standard input with timeout
```

 IN:   BC = timeout in centiseconds

 OUT:  Fc = 0, success
               A  = character read
               BC = remaining time
       Fc = 1, error

 DIF:  AFBC..../....

```
## OS_Uash ($BE06) : update application static handle
```

 IN:   IX = current application
       BC = new application static handle

 OUT:  Fc = 0, success
       Fc = 1, A = RC_Hand

 DIF:  .F....../....

 NOTE: 
       implemented in OZ4.6
       call used to update application static andle in an active process
       when another application has been removed

```
## OS_Use ($EE06) : fetch information about process card usage
```

 IN:   IX = application static handle

 OUT:  Fc = 0, success, A = running slot (0-4, 4 = :APP.- device)
       Fc = 1, A = RC_Hand

 DIF:  AF....../..../afbcdehl

```
## OS_Ust ($78) : Update small timer
```

 IN :  BC = new value

 OUT:  BC = old value
       Fz = 1, timeout reached

 DIF:  .FBC..../..../afbcdehl

 NOTE: 
       About Small Timer
       -----------------

       Small timer decrements on each 10ms tick, it is managed by low ram interrupt.
       It is used internally for API with timeout (OS_Tin, OS_Si, OS_Dly...)
       When BC = 0, timer is stopped, countdown has ended and ITSK_B_TIMER flag is set
       When BC = -1, timer is inactive

       About OS_Ust
       ------------

       Call rewritten for kernel 0 without OS push frame usage.
       With the same API, it provides a better reactivity with less overhead.
       OS_Ust is a call used by Pipedream to test and manipulate small timer.

```
## OS_Wait ($7E) : snooze until system event
```

 IN : -
 OUT: -
 DIF: AF....../..../afbcdehl

 NOTE: 
       Call rewritten for kernel 0 without OS push frame usage.
       With the same API, it provides a better reactivity with less overhead.
       Entering/exiting the snooze state is really fast and provides an optimal power management.
       Serial port activity is scanned first, if connected, OS_Wait wait for incoming bytes
       and snooze cannot be initiated.
       Therefore, leaving the serial port connected will consume batteries.

```
## OS_Wbe ($A5) : write byte at extended address
```

 IN:   BDE = extended address, B = 0, local
       A   = value to be written

 OUT:  -

 DIF:  .F....../....

 NOTE: implemented in OZ5.0 as a faster GN_Wbe replacement

```
## OS_Wrt ($CC06) : write token
```

 IN:   A = token

 OUT:  -

 DIF:  AF....../..../afbcdehl

```
## OS_Wsq ($CE06) : write to prefix sequence
```

 IN:   A=char, HL=sequence buffer

 OUT:  Fc=0, always, A = status
               A = 0  if sequence completed
               A = -1 otherwise

 DIF:  AF....../..../afbcdehl

```
## OS_Wtb ($CA06) : write token base
```

 IN:   BHL = new token base

 OUT:  BHL = old token base

 DIF:  AFB...HL/..../afbcdehl

```
## OS_Wts ($BC06) : write token string
```

 IN:   BHL = tokenized string to be displayed (B = 0, local)

 OUT:  Fc = 0, always

 DIF:  .F....../..../afbcdehl

 NOTE: call implemented in OZ5.0

       this call support special sequences :
       - for OZ icon, use SOH($01) + SD commands (>127)
       - for ISO, use STX($02) + ISO (>159)

```
## OS_Xin ($30) : examine input
```

 IN :  -

 OUT:  Fc = 0, OS_In will return immediately possibly with error
       Fc = 1, A=RC_EOF ($09), OS_In will wait

 DIF:  AF....../....

```
# OZ calls : low ram vectors
```

       Those OZ low level calls are located in low ram.
       They provide useful and responsive core functions.
       They are available from any segment using an RST or CALL instruction.

```
## EXTCALL (RST $28) : 24bit call subroutine in external bank
```

 This routine is executed by the RST 28H vector. 
 The 24bit address is available in low byte - high byte order following the RST 28H
 instruction opcode. The segment specifier (bits 15,14) of the lower 16bit address
 automatically identifies where the bank will be bound into the Z80 address space.
 The bank number of the 32bit address is specified as absolute (00 - $FF),
 identifying a 16K bank in 4Mb range.

 The ExtCall does not destroy any register call arguments or return values.
 ExtCall is to be regarded as a normal CALL instruction, but for 24 address range.

 Example in Z80 assembler, macro notation: EXTCALL $C000,$FE
 Execute code in bank $FE at address $C000 bound into segment 3
 (the instruction opcode sequence in memory is $EF, $00, $C0, $FE).
 This call instruction uses an absolute bank (located in slot 3) in the 24bit address,
 i.e. a piece of code in slot 2 might want to execute a subroutine in slot 3.

```
## INT ($0038) : maskable interrupt
```

 NOTE:
       About INT priority
       ------------------

       Z88 interruptions are set in MODE 1 (IM 1) during reset. 
       Any interruption calls address $0038. Dispatcher is located in low RAM.
       10ms tick (TSTA.TICK) and RX serial port (UIT.RDRF) interrupts have the highiest priority.
       They're processed in low RAM.
       - tick interrupt manages the small timer (see OS_Ust), beeper and keyboard.
       - RX interrupt fills the RX buffer.
       - TX, DCD, CTS serial port and A19 interrupts are ignored.

       About defered interruptions
       ---------------------------

       Minute, second, batlow interrupts are implemented as defered interrupts.
       Flap (card manager) is also a defered interrupt.
       Code execution is defered on the return of the interrupt, during defered execution,
       interrupts are enabled for highest priority interrupt to be handled (i.e. serial port)
       Thus, defered interruption must be acknowledged BEFORE entering the defered code
       (avoiding nested and endless execution).
       They're located in kernel and accessed by a special call (JpK0HL).

       This implementation solved the serial port glitch errors due to time interrupts.

```
## NMI ($0066) : non maskable interrupt
```

       This interruption is always fired when :
       - a power failure occurs (Vcc < 3.2V)
       - SNS pin goes low (card is being inserted)

       Coma state is entered by a HALT instruction :
       - LCD is shut down
       - switched power rails is shut down
       - master clock (9.8304 MHz) is shut down
       - standby clock is switched on (25.6 KHz)

       Minimal circuitry is maintained to trigger a maskable interruption (INT)
       which will revive the machine.

```
## CALL OZ_DI ($0051) : disable maskable interruptions
```

 IN:   -

 OUT:  Fc = previous status
               Fc = 0, maskable interruptions were enabled
               Fc = 1, maskable interruptions were disabled

 DIF:  .F....../..../........

 NOTE:
       usage of this call is mandatory due to hardware bug
       previous maskable interrupt state is returned in Fc
       and should be saved by PUSH AF or EX AF,AF'

       Zilog Q&A Note 3-130 : all NMOS Z80 have the LD A,I bug
       -------------------------------------------------------

       Zilog report that this bug has been fixed in CMOS Z80 but some
       early CMOS Z80 still have the LD A,I bug (Z84C0004PSC 8714 datestamp, confirmed by Martin Roberts)
       
       LD A,I  ; P/V contains contents of IFF2 (P/V = 0 = PO ; P/V = 1 = PE)

       BUG: if an interrupt occurs during execution of this instruction, the Parity flag contains a 0

```
## CALL OZ_EI ($0054) : enable maskable interruptions
```

 IN:   Fc = previous status

 OUT:  -

 DIF:  ......../..../........

 NOTE:
       call used to enable interruptions when disabled by OZ_DI
       interruption are effectively enabled if previous state was enabled (Fc = 0)

```
## CALL OZ_MGB ($0057) : memory get binding
```

 IN:   C = memory segment

 OUT:  B = bank bound in segment specified

 DIF:  ..B...../..../........

 NOTE:
       get current Bank binding for specified segment MS_Sx, defined in C
       this is the functional equivalent of OS_MGB, but much faster

```
## RST OZ_MPB (RST $30) : memory put binding
```

 IN:   B = bank (0-255)
       C = segment (0-3)

 OUT:  B = previous bank

 DIF:  ..B...../..../........
 
 NOTE:
       bind bank, defined in B, into segment C (MS_Sx), returns old bank binding in B
       this is the functional equivalent of original OS_MPB, but much faster

```
# Appendix
```

       This section refers to notes about conventions, structure, formats
       and architecture design.

```
## Application Execution
```

 NOTE:

       About Stack structure
       ---------------------

       $1FFE   stack top (user stack is saved here)
       ...     unsafe area
       ...     safe area
       ...     bad memory 320 bytes table (if bad or ugly application)
       ...     system stack expands downward
       $1800   stack bottom
       

       About Application static handle
       -------------------------------

       unique 16bit number refering to an application, only LSB is significant,
       generally hold in IX (IXH = 0) or A
       and bit 7 = ROM/APP, bits 5-6 = slot, bits 0-4 = number (not zero)

       01 -31  : slot 0 application (1 is Index)
       33 -63  : slot 1 application
       65 -95  : slot 2 application
       97 -127 : slot 3 application
       128-255 : installed application in RAM


       About environment on entry to an application
       --------------------------------------------

       environment informations are passed by OZ to an application on first entry,

       A   = running OZ version [implemented since OZ 5.0]
       C   = process id (Pid)   [implemented since OZ 5.0]
       DE  = stack bottom ($1800, always) [used by Pipedream]
       BHL = 0
       IX  = contigous memory info block (only valid for bad/ugly applications)
               (IX+0)  3       length of info block
               (IX+1)  $20     bad/ugly memory origin (MSB only, $2000, always)
               (IX+2)  x       bad/ugly memory allocated (in pages, up to $C0, 40K)
               (IX+3)  0       reserved

       Bad application must provide a response to an enquiry for a partial memory release when pre-empted.
       Bad application entry has to be a jump followed by the enquiry entry point.
       Enquiry should return :
                       Fc = 1 if no memory to release
                       Fc = 0 if a memory hole has to be released and
                               BC = hole start
                               DE = hole end

       Good application and popdowns (good or ugly) entries may begin with any code.


       About environment overhead and stacked process
       ----------------------------------------------

       this system variable (uwAppEnvOverhead) is defined in application DOR.
       it defines the size of the pseudo-file (OP_MEM) associated to a stacked process.
       this file has 3 headered parts :
               PE_VAR : system application variable area
               PE_STK : stack image containing
                        - safe variable area
                        - bad memory table (if existing)
                        - application stack
               PE_SCR : user screen

       environment overhead is automaticaly updated by OS_Stk.
       it should be always set to 0 in application DOR.

```
## Bad and Ugly applications
```

 NOTE:

       About bad memory table
       ----------------------

       Bad memory table is a 320 bytes (160 words) area.
       It contains the bad (contiguous) memory mapping (up to 40K)
       This table is saved in the stacked process under the PE_STK (stack) id.
       Each word contains page and bank mapping.
       It is generally addressed via (IY+0), (IY+1)

       (IY+0)  bit 7   : FREE_THIS flag, mark as page used
               bit 6   : reserved, always 0
               bit 5-0 : MSB of page allocated

       (IY+1)  = 0 : unused
               = 1 : page is in swap (page MSB is $00 or $80)
               >31 : bank of page allocated

       About swap
       ----------

       The swap area is a contiguous memory area : 8K on unexpanded and 40K
       on expanded machines.
       
                MS0   MS1   MS2
       Slot 0 : $20U  $22   $23
       Slot 1 : $40   $41   $42
       Slot 2 : $80   $81   $82
       Slot 3 : $C0   $C1   $C2

       Swap area is allocated by bad and ugly applications.
       Internal states are as follows :
       1) SwapOpen : memory is allocated as requested by application (8/40K)
       2) SwapRevive : content of swap is copied to memory allocated
                       the swap area is ready for application usage
       3) SwapSuspend/SwapRevive : called if process is pre-empted
       4) SwapClose : SwapSuspend is performed to return swap in previous state
                      pages allocated are released and memory pool is closed

```
## OZ Filesystem
```

 NOTE :
       About sector format
       -------------------

       The filesystem is organized as sectors of 64 bytes (4 by page).
       Each sector has a 16bit link (an fsPtr) followed by 62 bytes 
       available for data.
       In last sector, the link is $00ll with
               LSB = size of last sector (0-62)
               MSB = 0

       About fsPtr description
       -----------------------

       The filesystem pointer points to a sector.
       It simplifies operations and avoid 24bit pointer usage.
       It is generally passed in DE
       D = bank number and if :
               D = 0, last sector tag
               D = 1, deletion tag
       E = pointer to sector (rearranged to 10eeeeee ee000000)

```
## HIRES font
```

 Up to 1024 hires characters can be displayed by the blink
 768 ($300) from the hires0 bitmap font defined in RAM (granularity 8K)
 256 ($100) from the hires1 bitmap font defined in ROM (granularity 2K)
 only 128 are used in hires1, beginning at 128 ($80)
 that is why hires1 offset is ORGed 1K after ($80*8)
 blink hires1 is setup at $0800
 the free area is used for lowram binary image (1K)

```
## ISO character to font table
```

 NOTE:
       About ISO to font translation used by NQ_RDS
       --------------------------------------------

       ISO                     FONT
       ---                     ----
       $80-$9F  black square   $120
       $A0-$BF  symbols        $000-$01F
       $C0-$DF  upper          $080-$09F
       $E0-$FF  lower          $100-$11F

       $20-$7F  ascii normal   $020-$07F
       $20-$7F  ascii bold     $0A0-$0FF
       $20-$7F  ascii tiny     $120-$17F

       $00-$3F  oz symbols     $180-$1BF
       $00-$3F  UDG            $1C0-$1FF

```
## Keyboard maps
```

 Keymaps are located at page boundary. Each keymap fit in one page.
 First table is the global localization table pointing to localized keymaps.

 A keymap contains 6 tables :
       single key table
       shift table
       caps table
       control (diamond) table
       alternate (square) table
       deadkey table with none to several subtables

 structure of shift, square, and diamond tables:

       ds.b n                    number of character pairs in table
       ds.b inchar,outchar       translates inchar into outchar
       ds.b inchar,outchar,...   entries are ordered in ascending inchar order

 capsable table:

       ds.b n                    number of character pairs in table
       ds.b lcase,ucase          translates lcase into ucase and vice versa
       ds.b lcase,ucase,...      entries can be unsorted, but why not sort them?

 structure of deadkey table:

       ds.b n                    number of deadkeys in table
       ds.b keycode,offset       keycode of deadkey, offset into subtable for that key
       ds.b keycode,offset,...   offset is table address low byte
                                 entries are ordered in ascending keycode order

       ds.b char                 deadkey subtables start with extra byte - 8x8 char code for OZ window
       ds.b n                    after that they follow standard table format of num + n*(in,out)
       ds.b inchar, outchar,...

```
## LORES font
```

 up to 512 lores (6 pixels wide) characters can be displayed by the blink
 448 ($1C0) from the lores1 bitmap font defined in ROM (granularity 4K)
  64 ($40)  from the lores0 bitmap font defined in RAM (granularity 512B)

 full ISO Latin-9
 ----------------

 organised in 4 parts : 128 + 128 + 128 + 64 in order to follow the respecting ASCII number
       part 1 : 32 symbol iso + 96 normal ascii font
       part 2 : 32 upper iso  + 96 bold   ascii font
       part 3 : 32 lower iso  + 96 tiny   ascii font
       part 4 : 64 oz symbol font

```
## OSFRAME : OS stack frame
```

       OSFrame is a byte pattern pushed prior 2 bytes RST 20h calls (OS, DC, GN).
       It contains the main register values and memory binding.
       IY refers to the stack pointer and OSFrame is accessed by IY+dd instructions.
       
       +00h    IY
       +02h    S2S3 (S2 LSB, S3 MSB, used to restore S2)
       +04h    OZ call
       +06h    AF
       +08h    BC
       +0Ah    DE
       +0Ch    HL
       +0Eh    S3S3 (S2 LSB, S3 MSB, used to restore S3)
       +10h    RET

```
## OZ Window
```

 NOTE:
       About OZ window layout
       ----------------------

       ROW   MESSAGE
       1     OZ / LOCKOUT / MENU / HELP / WAIT icon
       2     localization (2 letters)
       3     CLI
       4     alarm bell
       5     hour | command |
       6     :    | command | bat
       7     min  | command | low
       8     caps | command |

```
## Printer filter
```

       
       The printer filter is accessed by the ENQ (5) prefix.
       Supported control characters are :
       - BEL (7)       bell
       - LF  (10)      line feed
       - FF  (12)      form feed
       - CR  (13)      carriage return
       - ESC (27)      escape

       Following prefix sequences controls the printer :
       - ENQ, '['              printer on  (soft reset the serial port and enable printer)
       - ENQ, ']'              printer off (disable printer)
       - ENQ, 'S'              reset attributes (all attributes are off)
       - ENQ, '2', 'H', nn     set microspace width (nn = $20 + value)
       - ENQ, '2', 'P', nn     set page length (nn = $20 + lines)

       Attribute toggle is set or reset by the sequence :
       - ENQ, 'U'              underline
       - ENQ, 'B'              bold
       - ENQ, 'X'              extended
       - ENQ, 'I'              italics
       - ENQ, 'L'              subscript
       - ENQ, 'R'              superscript
       - ENQ, 'A'              alternate font
       - ENQ, 'E'              user defined

```

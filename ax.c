// AX
// my Z80 assembler API extractor
//
// T.Peycru, 2023-25

// NOTE:
// -----
//
// API extractor will extract all the comment lines following ;@/ 
//
// The string following the trigger sequence of LF+';'+'@'+'/'
// is the sorting label of the paragraph of comments
//
// All the files in the input directory and its childhood are scanned
// Output is sorted on the label used to build the TOC
//
// Output format is Latex or Markdown
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <sys/errno.h> // lets us directly access errno
#include <fts.h>
#include "ax.h"

//
// function returning the level of a string
// ----------------------------------------
//
// counts the number of '/' occurences in the string
// 1 : chapter
// 2 : section
// 3 : subsection
//
int GetLevel(char *ptr)
{
    int k;
    int j = 0;

    for(k = 0; k<strlen(ptr); k++)
    {
        if( ptr[k] == '/' ) j++;
    }
    return j;
}

//
// function to generate anchor link
// --------------------------------
//
// text is converted to lowercase
// all non-text is removed
// spaces are converted to hyphens
// two-or more hyphens are converted to one
//
int GenAnchor(char *dest, char *src)
{
    int c;
    int d  = 0;
    int sp = 0;
    for(c=0; c<strlen(src); c++)
    {
        switch (src[c])
        {
            case ' ':
                {
                    if (!sp)
                    {
                        dest[d] = '-';
                        d++;  
                    } 
                    sp = 1;
                    break;  
                }
            case 'a' ... 'z':
            case '0' ... '9':
            case '_':
            case '-':
                {
                    dest[d] = src[c];
                    d++;
                    sp = 0;
                    break;
                }
            case 'A' ... 'Z':
                {
                    dest[d] = (char) tolower(src[c]);
                    d++;
                    sp = 0;
                    break;
                }                
        }
    }
    dest[d] = 0; // null terminator
    return 0;
}

//
// function to extract API comments in a given filename
// ----------------------------------------------------
//
// trigger is a CRLF+";@/" followed by entry name (to be sorted in a TOC)
// next lines with comment (beginning with ';') are entry text
// any other line ends the text
//
int ApiExtract(char *argv)
{
    // file to parse
    FILE *fp = fopen(argv, "r");
    if(fp == NULL) return 0;

    // state : 0=detect mode, 1=scanning mode
    int scanning = 0;

    // sizes
    int rsz = 0;
    int asz = 0;
    int nsz = 0;
    int tsz = 0;

    // buffers
    char chunk[C_MAX];
    char rbuf[R_MAX];
    char abuf[A_MAX];
    char nbuf[N_MAX];
    char tbuf[T_MAX];

    // reset buffers
    memset(chunk, 0, C_MAX);
    memset(rbuf,  0, R_MAX);
    memset(abuf,  0, A_MAX);
    memset(nbuf,  0, N_MAX);
    memset(tbuf,  0, T_MAX);

    while(fgets(chunk, C_MAX, fp) != NULL)
    {
        if (!scanning)
        {
            // detect mode, look for trigger
            // trigger sequence for API name
            if (chunk[0] == ';' && chunk[1] == '@' && chunk[2] == '/')
            {
                // trigger found, switch to scanning mode
                scanning = FSM_LINE;
                // store reference (for TOC and entry sorting)
                strncpy(rbuf, &chunk[2], R_MAX-1);
            }
        }
        else
        {
            // scanning mode
            if (chunk[0] == ';')
            {
                if (scanning == FSM_LINE)
                {
                    // store name (title) of comment
                    // exclude ; - and ; alone
                    if (strlen(chunk)>8 && chunk[2]!='-')
                    {
                        strncpy( nbuf, &chunk[8], N_MAX-1);
                        GenAnchor( abuf, &chunk[8]);
                        scanning = FSM_TEXT;
                    }    
                }
                else
                {
                    // append line of text
                    strncat( tbuf, &chunk[1], T_MAX-strlen(tbuf)-1);    
                }                
            }
            else
            {
                // text done, store name and text in a new entry
                ndx[i] = (struct entry *) calloc(1, sizeof(struct entry));

                // set allocation sizes
                rsz = strlen(rbuf)+1;
                asz = strlen(abuf)+1;
                nsz = strlen(nbuf)+1;
                tsz = strlen(tbuf)+1;

                // allocate and store reference
                ndx[i]->ref = (char *) calloc(rsz, sizeof(char));
                strncpy( ndx[i]->ref, rbuf, rsz);

                // allocate and store anchor
                ndx[i]->anch = (char *) calloc(asz, sizeof(char));
                strncpy( ndx[i]->anch, abuf, asz);

                // allocate and store name
                ndx[i]->name = (char *) calloc(nsz, sizeof(char));
                strncpy( ndx[i]->name, nbuf, nsz);

                // allocate and store text
                ndx[i]->text = (char *) calloc(tsz, sizeof(char));
                strncpy(ndx[i]->text, tbuf, tsz);

                // clear buffers for next iteration
                memset(rbuf, 0, R_MAX);
                memset(abuf, 0, A_MAX);
                memset(nbuf, 0, N_MAX);
                memset(tbuf, 0, T_MAX);

                // next entry
                i++;
                if (i>I_MAX) return -1;

                // switch to detect mode
                scanning = 0;
            }
        }
    }
    fclose(fp);
    return 0;
}


//
// function replacing latex special characters
// -------------------------------------------
//
char *StrTex(char *tin, char *tout)
{
    int i = 0;
    int o = 0;
    char j;
    memset(tout,  0, T_MAX);
    for(i=0; i<T_MAX; i++)
    {
        j = tin[i];
        if(j == '_' || j == '$' || j == '&')
        {
            tout[o] = '\\';
            o++;
        }
        tout[o] = j;
        o++;
        if(!j) break;
    }
    return tout;
}


//
// function to process entries and output API in Latex
// ---------------------------------------------------
//
int ApiOutputTex(FILE *fp, bool debugoutput)
{
    int l = 0;
    int m = 0;
    int n = 0;
    int o;
    struct entry *t;
    char *u;
    char *texlevel[] = {"chapter", "section", "subsection", "subsubsection"};
    int v;
    char texbuf[T_MAX];

    // remove LF in references and names
    for(l=0; l<i; l++)
    {
        u = strchr(ndx[l]->ref, '\n');
        if (u) memset(u, 0, 1);
        u = strchr(ndx[l]->name, '\n');
        if (u) memset(u, 0, 1);
    }

    //debug output
    if (debugoutput)
    {
        for(l=0; l<i; l++)
        {
            printf("%04u  %-24s  %s\n",l,ndx[l]->ref,ndx[l]->anch);
        }
    }

    // sort array
    for(m=0; m<i; m++)
    {
        for(n=m; n<i; n++)
        {
            if (strcmp(ndx[n]->ref, ndx[m]->ref)<0)
            {
                t = ndx[m];
                ndx[m] = ndx[n];
                ndx[n] = t;
            }    
        }       
    }

    // output all
    for(l=0; l<i; l++)
    {
        // output name
        v = GetLevel(ndx[l]->ref);
        fprintf(fp, "\\%s{%s}\n", texlevel[v-1], StrTex(ndx[l]->name, texbuf));

        // output text
        if ( strlen(ndx[l]->text) > 1 )
        {
            //if (v > 1)
            //{
                //use lstlisting everywhere
                fprintf(fp, "\\begin{lstlisting}\n");
                fprintf(fp, "%s\n\n", ndx[l]->text);
                fprintf(fp, "\\end{lstlisting}\n");
            //}    
            //else
            //{
            //    fprintf(fp, "%s\n\n", StrTex(ndx[l]->text, texbuf));
            //}            
        }
    }
    return 0;
}

//
// function to process entries and output API in Markdown
// ------------------------------------------------------
//
int ApiOutputMd(FILE *fp, bool debugoutput)
{
    int l = 0;
    int m = 0;
    int n = 0;
    int o;
    struct entry *t;
    char *u;

    // remove LF in references
    for(l=0; l<i; l++)
    {
        u = strchr(ndx[l]->ref, '\n');
        if (u) memset(u, 0, 1);
    }

    //debug
    if ( debugoutput )
    {
        for(l=0; l<i; l++)
        {
            printf("%04u  %-24s  %s\n",l,ndx[l]->ref,ndx[l]->anch);
        }
    }

    // sort array
    for(m=0; m<i; m++)
    {
        for(n=m; n<i; n++)
        {
            if (strcmp(ndx[n]->ref, ndx[m]->ref)<0)
            {
                t = ndx[m];
                ndx[m] = ndx[n];
                ndx[n] = t;
            }    
        }       
    }

    // output references (TOC)
    fprintf(fp, "# OZ 5.0 System API References");
    n = 0;
    for(l=0; l<i; l++)
    {
        m = GetLevel(ndx[l]->ref);
        // LF between sections
        if (m == 1)
        {
            if (n == 0) fprintf(fp, "\n\n");
            else        fprintf(fp, "\n\n\n");
        }
        // output references if level == 1 or 2
        if (m < 3)
        {
            u = strrchr(ndx[l]->ref, '/')+sizeof(char);

            // output header id anchor
            // header depends on renderer (not standard)
            // for gitlab, (#...)
            // for bitbucket, (#markdown-header-...)
            fprintf(fp, "[`%s`](#%s)", u, ndx[l]->anch);
            if (m < 2)
            {
                fprintf(fp, "\n\n\n");
                n = 0;
            }
            else
            {
                if (n > 4)
                {
                    n = 0;
                    fprintf(fp, "\n\n");
                }
                else
                {
                    n++;
                    for(o=0; o<(16-strlen(u)); o++)
                    {
                        fprintf(fp, " &nbsp;");
                    }
                    
                }
            }
        }
    }
    // horizontal rule
    fprintf(fp, "\n\n---\n\n");

    // output text
    for(l=0; l<i; l++)
    {
        for(n=0; n<(GetLevel(ndx[l]->ref)); n++)
        {
            fprintf(fp, "#");
        }
        fprintf(fp, " %s", ndx[l]->name);
        if ( strlen(ndx[l]->text) > 1 )
        {
            fprintf(fp, "```\n");
            fprintf(fp, "%s", ndx[l]->text);
            fprintf(fp, "```\n");    
        }
    }
    return 0;
}

//
// release memory allocated for entries
// ------------------------------------
//
int ApiFreeMem(void)
{
    for(int l=0; l<i; l++)
    {
        free(ndx[l]->ref);
        free(ndx[l]->name);
        free(ndx[l]->text);
        free(ndx[l]);
        ndx[l] = 0;
    }
    return 0;    
}

//
// main parse current file tree using FTS
// --------------------------------------
//
int main(int argc, char *argv[])
{
    // parse command line for options
    // default : debug is off, output latex file
    size_t optind;
    bool debugoutput = false;
    enum { LX_MODE, MD_MODE } outputmode = LX_MODE;
    char *inpdir = ".";

    char outname[FN_MAX];
    char *outfile = "api";

    for (optind = 1; optind < argc && argv[optind][0] == '-'; optind++)
    {
        switch (argv[optind][1])
        {
        case 'd': debugoutput = true; break;
        case 'l': outputmode = LX_MODE; break;
        case 'm': outputmode = MD_MODE; break;
        case 'h':
            fprintf(stdout, "%s", help);
            exit(EXIT_SUCCESS);
        default:
            fprintf(stderr, "%s", usage);
            exit(EXIT_FAILURE);
        }   
    }
    
    // current argument is input directory
    if (optind < argc) inpdir = argv[optind];
    else 
    {
        fprintf(stderr, "%s", usage);
        exit(EXIT_FAILURE);        
    }

    // current argument is output file if existing
    if (++optind < argc) outfile = argv[optind];

    // generate output filename
    strncpy(outname, outfile, FN_MAX-5);

    // if a '.' is given don't add extension
    if ( !strchr(outname, '.') )
    {
        if (outputmode == MD_MODE) strcat(outname, MDEXT);
        else                       strcat(outname, LXEXT);
    }

    /* An array of paths to traverse. Each path must be null
     * terminated and the list must end with a NULL pointer. */
    // start from the current directory
    char *paths[] = { inpdir, NULL };
    
    /* 2nd parameter: An options parameter. Must include either
       FTS_PHYSICAL or FTS_LOGICAL---they change how symbolic links
       are handled.
       The 2nd parameter can also include the FTS_NOCHDIR bit (with a
       bitwise OR) which causes fts to skip changing into other
       directories. I.e., fts will call chdir() to literally cause
       your program to behave as if it is running into another
       directory until it exits that directory. See "man fts" for more
       information.
       Last parameter is a comparator which you can optionally provide
       to change the traversal of the filesystem hierarchy.
    */
    FTS *ftsp = fts_open(paths, FTS_PHYSICAL | FTS_NOCHDIR, NULL);
    if(ftsp == NULL)
    {
        perror("fts_open");
        exit(EXIT_FAILURE);
    }

    while(1) // call fts_read() enough times to get each file
    {
        FTSENT *ent = fts_read(ftsp); // get next entry (could be file or directory).
        if(ent == NULL)
        {
            if(errno == 0)
                break; // No more items, bail out of while loop
            else
            {
                // fts_read() had an error.
                perror("fts_read");
                exit(EXIT_FAILURE);
            }
        }
            
        // Given a "entry", call extractor if it is a file
        if (ent->fts_info & FTS_F) // The entry is a file.
        {
            if (ApiExtract(ent->fts_path))
            {
                // extraction error
                perror("api_x");
                exit(EXIT_FAILURE);
            }
        }
    }

    // close fts and check for error closing.
    if(fts_close(ftsp) == -1)
        perror("fts_close");

    // output
    FILE *fpout = fopen(outname, "w");
    if(fpout == NULL) return -1;

    // use output options
    if ( outputmode == MD_MODE )
    {
        // output as MD
        ApiOutputMd(fpout,debugoutput);
    }
    else
    {
        // output as LX
        fprintf(fpout, "%s", lead);
        ApiOutputTex(fpout,debugoutput);
        fprintf(fpout, "%s", tail);
    }
    // release memory
    ApiFreeMem();

    // close output
    fclose(fpout);
    return 0;
}
